/* Function to check for network connectivity */
document.addEventListener("deviceready", onDeviceReady, false);


var calendarDefault = myApp.calendar({
    input: '#calendar-default',
    input: '#calendar-default-expense',
            input: '#meeting_date',
});

var pictureSource; // picture source
var destinationType; // sets the format of returned value

// PhoneGap is ready
//
function onDeviceReady()
{
    pictureSource = navigator.camera.PictureSourceType;
    destinationType = navigator.camera.DestinationType;

    var deviceOS = device.platform;  //fetch the device operating system
    var deviceOSVersion = device.version;  //fetch the device OS version
    var version = parseInt(deviceOSVersion);

    if ((deviceOS == 'Android') && (version >= 6))
    {
        cordova.plugins.diagnostic.requestRuntimePermissions(function (statuses) {
            for (var permission in statuses) {
                switch (statuses[permission]) {
                    case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                        navigator.geolocation.getCurrentPosition(on_location_success_home, on_location_error, {maximumAge: 3000, timeout: 5000, enableHighAccuracy: true});
                        //alert("Permission granted to use "+permission);
                        break;
                    case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                        //alert("Permission to use "+permission+" has not been requested yet");
                        break;
                    case cordova.plugins.diagnostic.permissionStatus.DENIED:
                        //alert("Permission denied to use "+permission+" - ask again?");
                        break;
                    case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                        //alert("Permission permanently denied to use "+permission+" - guess we won't be using it then!");
                        break;
                }
            }
        }, function (error) {
            console.error("The following error occurred: " + error);
        }, [
            cordova.plugins.diagnostic.permission.ACCESS_FINE_LOCATION,
            cordova.plugins.diagnostic.permission.ACCESS_COARSE_LOCATION
        ]);

    }

    else
    {
        cordova.plugins.diagnostic.isLocationAvailable(function (available)
        {
            if (available == true)
            {
                navigator.geolocation.getCurrentPosition(on_location_success_home, on_location_error);
            }

            else
            {
                myApp.alert('Please turn on your GPS and restart the app', app_name, function () {
                    navigator.app.exitApp();
                });
            }
        }, function (error)
        {
            myApp.alert("The following error occurred: " + error);
        });
    }
    //back button listener
    document.addEventListener("backbutton", onBackKeyDown, false);

    //push notifications
    var push = PushNotification.init({"android": {"senderID": "540631623027"},
        "ios": {"alert": "true", "badge": "true", "sound": "true"}, "windows": {}});

    push.on('registration', function (data) {
        // data.registrationId
        //console.log(data.registrationId);
        //$("#gcm_id").html(data.registrationId);
        window.localStorage.setItem("iod_registrationId", data.registrationId);
        service.add_registration_id(data.registrationId).done(function (employees) {
            var data2 = jQuery.parseJSON(employees);

            console.log(data2);
        });
    });

    push.on('notification', function (data) {
        // data.message,
        // data.title,
        // data.count,
        // data.sound,
        // data.image,
        // data.additionalData
        console.log(data.message);
        //myApp.alert(data.title+" Message: " +data.message, 'Choto');
    });

    push.on('error', function (e) {
        // e.message
        console.log(e.message);
        //myApp.alert(e.message, 'Error');
    });
}
function get_client_navigation_status()
{
    navigator.geolocation.getCurrentPosition(on_location_success, on_location_error);
}

var on_location_success_home = function (position)
{
    app_latitude = position.coords.latitude;
    app_longitude = position.coords.longitude;
};

var on_location_success = function (position)
{
    app_latitude = position.coords.latitude;
    app_longitude = position.coords.longitude;

    window.localStorage.setItem("client_app_latitude", app_latitude);
    window.localStorage.setItem("client_app_longitude", app_longitude);


};

function start_visit()
{
    var app_latitude = window.localStorage.getItem("client_app_latitude");
    var app_longitude = window.localStorage.getItem("client_app_longitude");

    myApp.showPreloader('Loading Details...');
    service.start_visit_journey(app_latitude, app_longitude).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {
            app_address = data.result;
            document.getElementById("bottomid").style.display = "block";
            document.getElementById("error_location").style.display = "none";
            document.getElementById("close_visit").style.display = "block";

            var company_id = window.localStorage.getItem("company_id");
            window.localStorage.setItem("client_active", company_id);


        }

    }).fail(function (jqXHR, textStatus, errorThrown)
    {
        myApp.hidePreloader();
        myApp.alert('Please check your location, you are not close to the premise', 'Salesforce APP');
    });


}
var on_location_success_visit_start = function (position)
{
    app_latitude = position.coords.latitude;
    app_longitude = position.coords.longitude;
    myApp.showPreloader('Starting visit...');

    service.start_visit_journey(app_latitude, app_longitude).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {
            app_address = data.result;
            document.getElementById("bottom").style.display = "block";
            document.getElementById("error_location").style.display = "none";
            document.getElementById("close_visit").style.display = "block";

        }

    }).fail(function (jqXHR, textStatus, errorThrown)
    {
        myApp.hidePreloader();
        myApp.alert('Please check your location, you are not close to the premise', 'Salesforce APP');
    });

};
// onError Callback receives a PositionError object
//
function on_location_error(error) {
    /*alert('code: '    + error.code    + '\n' +
     'message: ' + error.message + '\n');*/

    service.reverse_geocode(app_latitude, app_longitude).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            app_address = data.result;
            alert(app_address);

        }
    }).fail(function (jqXHR, textStatus, errorThrown)
    {
    });
}

function onBackKeyDown() {
    // Handle the back button
    myApp.showIndicator();

    mainView.router.back();
    mainView.router.refreshPage();

    myApp.hideIndicator();
}

function onMenuKeyDown() {
    myApp.alert('Hello');
}

$(document).ready(function ()
{
    automatic_login();
    // get_all_zones();


});
function automatic_login()
{
    window.localStorage.clear();

    var logged_in = window.localStorage.getItem("logged_in_status");
    // myApp.alert(logged_in);
    if (logged_in == 'yes')
    {

        mainView.router.loadPage('main.html');


    }
    else
    {

        myApp.popup('.login-screen');
    }
    // mainView.router.loadPage('main.html');
}



function HideModalPopup()
{
    $("#ModalBehaviour").hide();
}

//Login member
$(document).on("submit", "form#login_member", function (e)
{
    e.preventDefault();
    myApp.showIndicator();
    //get form values
    var login_member_no = $('#login_member_no').val();
    var login_member_password = $('#login_member_password').val();

    //check if there is a network connection
    var connection = true;//is_connected();

    if (connection === true)
    {
        service.login_member(login_member_no, login_member_password).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.status == "success")
            {
                window.localStorage.setItem("personnel_id", data.message.personnel_id);
                window.localStorage.setItem("personnel_onames", data.message.personnel_onames);
                window.localStorage.setItem("logged_in_status", 'yes');
                window.localStorage.setItem("iod_member_no", data.message.perosnnel_number);
                window.localStorage.setItem("perosnnel_fname", data.message.perosnnel_fname);
                window.localStorage.setItem("job_title_id", data.message.job_title_id);

                var job_title_id = window.localStorage.getItem('job_title_id'); //data.message.job_title_id;

                // if (job_title_id != 1)
                // {

                    document.getElementById("bottom").style.display = "none";
                // }

                myApp.hideIndicator();


                // mainView.router.loadPage('main.html');
                myApp.alert('Welcome back ' + data.message.personnel_fname + ' ', 'Loyalty App');
                myApp.closeModal('.login-screen');

                get_promotion_listings();
                // get_all_zones();
                // get_all_vehicle_types();
            }
            else
            {
                myApp.hideIndicator();
                myApp.alert('' + data.message + '', 'Login Response');
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            myApp.hideIndicator();
            myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty Program');
        });
    }

    else
    {
        myApp.alert('No internet connection - please check your internet connection then try again', 'Login Error');
    }
    return false;
});


$$('.open-reminder').on('click', function () {
    myApp.popup('.popup-reminder');
});
//Login member
$(document).on("submit", "form#add_reminder", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    var form_data = new FormData(this);

    service.add_reminder(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // alert("changes of the close");

            myApp.hideIndicator();

            // document.getElementById("calendar-default").value = "";
            // document.getElementById("hour").value = "";
            // document.getElementById("min").value = "";
            // document.getElementById("subject").value = "";
            // document.getElementById("description").value = "";
            // myApp.closepopup('.popup-reminder');
            // myApp.closeModal('.popup');
            myApp.closeModal();

            // window.location.href ='reminders.html';

            mainView.router.loadPage('reminders.html');

            myApp.showIndicator();
            var personnel_id = window.localStorage.getItem("personnel_id");
            service.get_reminders(personnel_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {

                    $("#reminders-ul").html(data.result);
                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert(textStatus);
                    });


            myApp.hideIndicator();
        }
        else
        {
            myApp.hideIndicator();
            myApp.alert('' + data.message + '', 'Reminder Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});





//Login member
$(document).on("submit", "form#change_password", function (e)
{
    e.preventDefault();
    myApp.showIndicator();
    //get form values
    var form_data = new FormData(this);

    //check if there is a network connection
    var connection = checkConnection();

    //if(connection != 'No network connection')
    if (connection = true)
    {
        service.change_password(form_data).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                myApp.hideIndicator();
                window.localStorage.setItem("first_login", 'no');
                myApp.alert('Password changed successfully', 'IOD (Kenya)');

                mainView.router.loadPage('dist/dashboard.html');

            }
            else
            {
                myApp.hideIndicator();
                myApp.alert('' + data.result + '', 'IOD (Kenya)');
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            myApp.hideIndicator();
            myApp.alert(errorThrown + ' ' + textStatus, 'IoD (Kenya)');
        });
    }

    else
    {
        myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
    }
    return false;
});

//Handler for profile page click
$(document).on("click", "a.profile-page", function (e)
{
    e.preventDefault();

    var logged_in = window.localStorage.getItem('iod_logged_in');
    //window.localStorage.setItem('iod_logged_in', 'no');
    if (logged_in == "yes")
    {
        mainView.router.loadPage('profile.html');
    }

    else
    {
        myApp.loginScreen();
    }
});

function get_profile_details()
{
    $("#loader-wrapper").removeClass("display_none");

    service.getProfileDetails().done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // $( "#news-of-icpak" ).addClass( "display_block" );
            $("#my_profile").html(data.result);
            $("#loader-wrapper").addClass("display_none");
        }
    });
}

function set_news_data()
{

    service.getallLatesNews().done(function (employees) {

        var data = jQuery.parseJSON(employees);

        window.localStorage.setItem("news_history", data.result);
        window.localStorage.setItem("total_news", data.total);
    });
}
function load_messages()
{
    var messages = window.localStorage.getItem("news_history");
    $("#icpak_news").html(messages);
}

function change_to_arms()
{
    get_arms_items();
    window.location.href = "arms.html";
}

//Share post
$(document).on("click", "a.share_post", function (e)
{
    e.preventDefault();

    var message = $("#content").val();
    var subject = $("#title").val();
    var file = $("#image").val();
    var url = null;
    window.plugins.socialsharing.share(
            message,
            subject,
            file,
            url,
            function (result) {/*alert('success: ' + result)*/
            },
            function (result) {/*alert('error: ' + result)*/
            }
    );
    return false;
});

function display_comments(id)
{
    //get event comments
    var event_comments = window.localStorage.getItem("event_comments" + id);
    //var event_comments = null;

    if ((event_comments == "") || (event_comments == null) || (event_comments == "null"))
    {


        service.getNewsComments(id).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                window.localStorage.setItem("event_comments" + id, data.result);
                $("#forum_comments").html(data.result);
            }
        });
    }

    else
    {
        $("#event_comments").html(event_comments);


        service.getNewsComments(id).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                window.localStorage.setItem("event_comments" + id, data.result);
                $("#forum_comments").html(data.result);
            }
        });
    }
}

function refresh_blog_display(id)
{
    var total_blog = window.localStorage.getItem("total_blog" + id);
    var new_blog = window.localStorage.getItem("new_blog" + id);
    var first_load = window.localStorage.getItem("first" + id);

    //case of new adds
    if ((new_blog != total_blog) && (first_load == 'no'))
    {
        window.localStorage.setItem("total_blog" + id, new_blog);
        var blog_list = window.localStorage.getItem("blog_list" + id);
        $("#blog_details").html(blog_list);
    }
}

function get_blog_description(id)
{
    myApp.showIndicator();

    //get event details
    var blog_details = window.localStorage.getItem("blog_details" + id);
    var blog_details = null;

    if ((blog_details == "") || (blog_details == null) || (blog_details == "null"))
    {
        var member_id = window.localStorage.getItem("iod_member_id");

        service.getBlogDetail(id, member_id).done(function (employees) {
            var data = jQuery.parseJSON(employees);
            //alert('here');
            if (data.message == "success")
            {//alert(data.result);
                window.localStorage.setItem("blog_details" + id, data.result);
                window.localStorage.setItem("blog_title" + id, data.discussion_title);
                window.localStorage.setItem("blog_content" + id, data.discussion_content);
                window.localStorage.setItem("forum_post_id", id);
                $("#blog_details").html(data.result);
                $("h3#forum-title").html(data.discussion_title);
                myApp.hideIndicator();
            }

            else
            {
                myApp.alert('Oops. Unable to get content', 'ICPAK');
                myApp.hideIndicator();
            }
        });
    }

    else
    {
        setTimeout(function () {
            // change the page to home 
            var blog_title = window.localStorage.getItem("blog_title" + id);
            window.localStorage.setItem("post_id", id);
            $("#blog_details").html(blog_details);
            $("#forum-title").html(blog_title);
            myApp.hideIndicator();
        }, 3000);
    }

    var refresh_blog_selection2 = setInterval(function () {
        refresh_blog_timer(id)
    }, 2000);
    var refresh_blog_display2 = setInterval(function () {
        refresh_blog_display(id)
    }, 4000);
}

function refresh_blog_timer(id)
{
    var member_id = window.localStorage.getItem("iod_member_id");

    service.getBlogDetail(id, member_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            var total_blog = window.localStorage.getItem("total_blog" + id);
            var new_blog = data.total_received;
            window.localStorage.setItem("new_blog" + id, new_blog);

            if (total_blog != data.total_received)
            {
                window.localStorage.setItem("blog_list" + id, data.result);
            }
        }
    });
}

function refresh_blog_display(id)
{
    var total_blog = window.localStorage.getItem("total_blog" + id);
    var new_blog = window.localStorage.getItem("new_blog" + id);

    //case of new adds
    if (new_blog != total_blog)
    {
        window.localStorage.setItem("total_blog" + id, new_blog);
        var blog_list = window.localStorage.getItem("blog_list" + id);
        $("#blog_details").html(blog_list);
    }
}

//Login member
$(document).on("click", "a.post-notification", function (e)
{
    var post_id = window.localStorage.getItem("forum_post_id");
    var blog_title = window.localStorage.getItem("blog_title" + post_id);
    var blog_content = window.localStorage.getItem("blog_content" + post_id);
    myApp.addNotification({
        title: blog_title,
        message: blog_content
    });
});

//Login member
$(document).on("click", "a.add-comment", function ()
{
    myApp.showIndicator();

    //check if there is a network connection
    var connection = checkConnection();

    //if(connection != 'No network connection')
    if (connection = true)
    {
        var member_id = window.localStorage.getItem("iod_member_id");
        var comment = $("#post_comment").val();
        var post_id = window.localStorage.getItem("forum_post_id");
        service.add_comment(comment, post_id, member_id).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                myApp.hideIndicator();
                $("#post_comment").val('');
                get_blog_description(post_id);
            }
            else
            {
                myApp.hideIndicator();
                myApp.alert(data.result, 'Error');
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            myApp.hideIndicator();
            myApp.alert(errorThrown + ' ' + textStatus, 'IoD (Kenya)');
        });
    }

    else
    {
        myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
        myApp.hideIndicator();
    }
    return false;
});

//Login member
$(document).on("submit", "form#CommentForm", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    //check if there is a network connection
    var connection = checkConnection();

    //if(connection != 'No network connection')
    if (connection = true)
    {


        var member_id = window.localStorage.getItem("iod_member_id");
        var comment = $("textarea[name=comment]").val();
        var post_id = $("input[name=post_id]").val();
        service.add_comment(comment, post_id, member_id).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                myApp.hideIndicator();
                display_comments(post_id);
            }
            else
            {
                myApp.hideIndicator();
                myApp.alert(data.result, 'Error');
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            myApp.hideIndicator();
            myApp.alert(errorThrown + ' ' + textStatus, 'IoD (Kenya)');
        });
    }

    else
    {
        myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
        myApp.hideIndicator();
    }
    return false;
});

function refresh_forum_timer()
{
    var member_type_id = window.localStorage.getItem("member_type_id");

    service.get_forum_items(member_type_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            var total_forum = window.localStorage.getItem("total_forum");
            var new_forum = data.total_received;
            window.localStorage.setItem("new_forum", new_forum);

            if (total_forum != data.total_received)
            {
                window.localStorage.setItem("forum_list", data.result);
            }
        }

        else
        {
            myApp.hideIndicator();
            // myApp.alert("Please check your membership type then try again", 'Error');
            myApp.alert("Please log in as a member to access the forums", 'Error');


            mainView.router.loadPage('dist/dashboard.html');
        }
    });
}

function refresh_forum_display()
{
    var total_forum = window.localStorage.getItem("total_forum");
    var new_forum = window.localStorage.getItem("new_forum");
    var forum_list = window.localStorage.getItem("forum_list");

    if (forum_list == " ")
    {
        new_forum = total_forum;
        window.localStorage.setItem("total_forum", new_forum);
    }

    //case of new adds
    if (new_forum != total_forum)
    {
        window.localStorage.setItem("total_forum", new_forum);
        $("#all_forums").html(forum_list);
    }
}

$(document).on("submit", "form#ContactFormHe", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    //get form values
    var form_data = new FormData(this);

    //check if there is a network connection
    var connection = checkConnection();

    //if(connection != 'No network connection')
    if (connection = true)
    {

        service.post_contact_us(form_data).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                myApp.hideIndicator();
                myApp.alert("Thank you for contact us, we will address your issue and get back to you shortly.", 'IOD (Kenya)');

            }
            else
            {
                myApp.hideIndicator();
                myApp.alert(data.result, 'IOD (Kenya)');
            }
        });
    }

    else
    {
        myApp.hideIndicator();
        myApp.alert("No internet connection - please check your internet connection then try again", 'IOD (Kenya)');
    }
    return false;
});

//pass the variable in the link as follows e.g. news.html?id=1
//on the news.html page get the parameter by javascript as follows var id = getURLParameter('id');
//the function to get the url parameter is defined below
function getURLParameter(name)
{
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null
}


function refresh_initiatives_timer()
{
    service.getallinitiatives().done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            var total_initiatives = window.localStorage.getItem("total_initiatives");
            var new_initiatives = data.total_received;
            window.localStorage.setItem("new_initiatives", new_initiatives);

            if (total_initiatives != data.total_received)
            {
                window.localStorage.setItem("initiatives_list", data.result);
            }
        }
    });
}

function refresh_initiatives_display()
{
    var total_initiatives = window.localStorage.getItem("total_initiatives");
    var new_initiatives = window.localStorage.getItem("new_initiatives");

    //case of new adds
    if (new_initiatives != total_initiatives)
    {
        window.localStorage.setItem("total_initiatives", new_initiatives);
        var initiatives_list = window.localStorage.getItem("initiatives_list");
        $("#all_initiatives").html(initiatives_list);
    }
}

function get_initiative_page(id)
{
    myApp.showIndicator();
    //var sermon_details = null;
    var initiative_items = window.localStorage.getItem("initiative_items" + id);
    //initiative_items = null;
    if ((initiative_items == "") || (initiative_items == null) || (initiative_items == "null"))
    {


        service.getInitativePage(id).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                window.localStorage.setItem("initiative_items" + id, data.result);
                $("#initiative_items").html(data.result);
                myApp.hideIndicator();
            }

            else
            {
                myApp.hideIndicator();
                myApp.alert('Oops. Unable to get initiatives', 'IOD (Kenya)');
            }
        });
    }

    else
    {
        setTimeout(function () {
            // change the page to home 
            $("#initiative_items").html(initiative_items);
            myApp.hideIndicator();
        }, 3000);
    }
}

function get_initiatives_description(id, parent_id)
{
    myApp.showIndicator();
    //var sermon_details = null;
    var initiative_details = window.localStorage.getItem("initiative_details" + id);

    if ((initiative_details == "") || (initiative_details == null) || (initiative_details == "null"))
    {


        service.getInitiativeDetail(id, parent_id).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                window.localStorage.setItem("initiative_details" + id, data.result);
                $("#initiatives_detail").html(data.result);
                myApp.hideIndicator();
            }

            else
            {
                myApp.alert('Oops. Unable to get initiative', 'IOD (Kenya)');
                myApp.hideIndicator();
            }
        });
    }

    else
    {
        setTimeout(function () {
            // change the page to home 
            $("#initiatives_detail").html(initiative_details);
            myApp.hideIndicator();
        }, 3000);
    }
}

var myPhotoBrowserads = myApp.photoBrowser({
    photos: [
        'images/long-logo.png',
        'images/advertise-here.png',
        'images/advert-01.png',
    ]
});
//Open Home Slider photo browser on click
$$('.pb-slideshow').on('click', function () {
    myPhotoBrowserads.open();
});

$(document).on("click", "a.download-resource", function (e)
{
    e.preventDefault();
    myApp.showIndicator();
    var download_file = $(this).attr('download_file');
    //myApp.alert(download_file);
    //var ref = cordova.InAppBrowser.open('http://apache.org', '_blank', 'location=yes');
    window.open(download_file, '_system');
    myApp.hideIndicator();
    /*var fileTransfer = new FileTransfer();
     var uri = encodeURI(download_file);
     //var fileURL = cordova.file.dataDirectory;
     var fileURL =  "cdvfile://localhost/persistent/path/to/file.pdf";
     //myApp.alert(fileURL);
     
     fileTransfer.download(
     uri,
     fileURL,
     function(entry) {
     //myApp.alert("download complete: " + entry.toURL());
     var ref = cordova.InAppBrowser.open(entry.toURL(), '_blank', 'location=no,closebuttoncaption=Close,enableViewportScale=yes');
     //window.open(entry.toURL(), '_blank', 'location=no,closebuttoncaption=Close,enableViewportScale=yes');
     myApp.hideIndicator();
     },
     function(error) {
     myApp.alert("download error source " + error.source+" download error target " + error.target+" upload error code" + error.code);
     myApp.hideIndicator();
     },
     false,
     {
     headers: {
     "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
     }
     }
     );*/

    return false;
});

function get_single_post(id)
{
    myApp.showIndicator();

    var blog_items = window.localStorage.getItem("blog_item" + id);
    $("#blog_item").html(blog_items);

    service.get_blog_item(id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            $("#blog_item").html(data.result);
            window.localStorage.setItem("blog_item" + id, data.result);
        }
    });

    myApp.hideIndicator();
}

function get_event_details(id)
{
    myApp.showIndicator();
    //var sermon_details = null;
    window.localStorage.setItem("current_event_id", id);
    var event_details = window.localStorage.getItem("event_details" + id);

    var event_type_name = window.localStorage.getItem("event_type_name" + id);
    var event_name = window.localStorage.getItem("event_name" + id);
    var start_date = window.localStorage.getItem("start_date" + id);
    var end_date = window.localStorage.getItem("end_date" + id);
    var event_description = window.localStorage.getItem("event_description" + id);
    var event_timetable = window.localStorage.getItem("event_timetable" + id);
    var event_venue = window.localStorage.getItem("event_venue" + id);
    var event_latitude = window.localStorage.getItem("event_latitude" + id);
    var event_longitude = window.localStorage.getItem("event_longitude" + id);
    var event_time = window.localStorage.getItem("event_time" + id);

    if (event_name !== 'undefined')
    {
        $("#event_type_name").html(event_type_name);
        $("#event_name").html(event_name);
        $("#start_date").html(start_date);
        $("#end_date").html(end_date);
        $("#event_description").html(event_description);
        $("#event_timetable").html(event_timetable);
        $("#event_venue").html(event_venue);
        $("#event_time").html(event_time);
    }

    //generate_map(event_latitude, event_longitude);

    service.get_event_details(id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            event_type_name = data.result.event_type_name;
            event_name = data.result.event_name;
            event_web_name = data.result.event_web_name;
            start_date = data.result.start_date;
            end_date = data.result.end_date;
            event_description = data.result.event_description;
            event_timetable = data.result.event_timetable;
            event_venue = data.result.event_venue;
            event_latitude = data.result.event_latitude;
            event_longitude = data.result.event_longitude;
            event_time = data.result.event_time;

            window.localStorage.setItem("event_type_name" + id, event_type_name);
            window.localStorage.setItem("event_name" + id, event_name);
            window.localStorage.setItem("start_date" + id, start_date);
            window.localStorage.setItem("end_date" + id, end_date);
            window.localStorage.setItem("event_description" + id, event_description);
            window.localStorage.setItem("event_timetable" + id, event_timetable);
            window.localStorage.setItem("event_venue" + id, event_venue);
            window.localStorage.setItem("event_latitude" + id, event_latitude);
            window.localStorage.setItem("event_longitude" + id, event_longitude);
            window.localStorage.setItem("event_time" + id, event_time);

            $("#event_type_name").html(event_type_name);
            $("#event_name").html(event_name);
            $("#start_date").html(start_date);
            $("#end_date").html(end_date);
            $("#event_description").html(event_description);
            $("#event_timetable").html(event_timetable);
            $("#event_venue").html(event_venue);
            $("#event_latitude").html(event_latitude);
            $("#event_longitude").html(event_longitude);
            $("#event_time").html(event_time);

            //remove book now button if already booked
            var booking_status = window.localStorage.getItem("booking_status" + id);

            if (booking_status == 'booked')
            {
                $("button.book-now").css('display', 'none');
                var payment_status = window.localStorage.getItem("payment_status" + id);

                if (payment_status == 'unpaid')
                {
                    $("button.pay-now").css('display', 'block');
                }
            }

            generate_map(event_latitude, event_longitude);

            myApp.hideIndicator();
        }

        else
        {
            myApp.alert('Oops. Unable to get details', 'IOD (Kenya)');
            myApp.hideIndicator();
        }
    });
}

function generate_map(event_latitude, event_longitude)
{
    if ((event_longitude > 0) || (event_longitude < 0))
    {
        var my_latitude = event_latitude;
        var my_longitude = event_longitude;

        bounds = new google.maps.LatLngBounds();
        myLatlng = new google.maps.LatLng(my_latitude, my_longitude);
        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        //alert(event_longitude);
        var customer_marker = {
            url: 'images/customer-marker.png',
            // size: new google.maps.Size(63, 56),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(20, 64)
        };

        myLatlng = new google.maps.LatLng(my_latitude, my_longitude);

        map_center_marker = new google.maps.Marker({
            icon: customer_marker,
            position: myLatlng,
            map: map,
            animation: google.maps.Animation.DROP,
            title: 'Me'
        });
    }
}

//Book Event
$(document).on("click", "button.book-now", function (e)
{
    myApp.showIndicator();
    window.localStorage.setItem('iod_logged_in', 'no');
    var logged_in = window.localStorage.getItem('iod_logged_in');

    myApp.closeModal('.popup-events');
    if (logged_in == "yes")
    {
        var member_id = window.localStorage.getItem("iod_member_id");
        book_member(member_id);
        myApp.hideIndicator();
    }

    else
    {
        window.localStorage.setItem('iod_login_refferal', 'booking');
        mainView.router.loadPage('book.html');
        myApp.hideIndicator();
    }
});

//Login member
$(document).on("submit", "form#book-non-member", function (e)
{
    e.preventDefault();
    myApp.showIndicator();
    var form_data = new FormData(this);

    //check if there is a network connection
    var connection = true;//checkConnection();

    //if(connection != 'No network connection')
    if (connection = true)
    {
        service.book_non_member(form_data).done(function (employees) {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                myApp.hideIndicator();
                myApp.closeModal('.popup-non-member');
                myApp.alert('Booking successfully', 'IOD (Kenya)');

                var event_id = window.localStorage.getItem("current_event_id");

                window.localStorage.setItem("booking_id" + event_id, data.booking_id);
                window.localStorage.setItem("booking_cost" + event_id, data.booking_details.booking_cost);
                window.localStorage.setItem("booking_date" + event_id, data.booking_details.event_start_time);
                window.localStorage.setItem("booking_event" + event_id, data.booking_details.event_name);
                window.localStorage.setItem("booking_location" + event_id, data.booking_details.event_location);
                window.localStorage.setItem("booking_number" + event_id, data.booking_details.booking_number);
                window.localStorage.setItem("booking_name" + event_id, data.booking_details.name);

                window.localStorage.setItem("booking_status" + event_id, 'booked');
                window.localStorage.setItem("payment_status" + event_id, 'unpaid');

                load_event_page();
            }
            else
            {
                myApp.hideIndicator();
                myApp.alert('' + data.result + '', 'IOD (Kenya)');
            }
        });
    }

    else
    {
        myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
    }
    return false;
});

function book_member(member_id)
{
    myApp.showIndicator();
    //check if there is a network connection
    var connection = true;//checkConnection();

    //if(connection != 'No network connection')
    if (connection = true)
    {
        service.book_member(member_id).done(function (employees)
        {
            var data = jQuery.parseJSON(employees);

            if (data.message == "success")
            {
                myApp.hideIndicator();

                myApp.alert('Booking successfully', 'IOD (Kenya)');

                var event_id = window.localStorage.getItem("current_event_id");

                window.localStorage.setItem("booking_id" + event_id, data.booking_id);
                window.localStorage.setItem("booking_cost" + event_id, data.booking_details.booking_cost);
                window.localStorage.setItem("booking_date" + event_id, data.booking_details.event_start_time);
                window.localStorage.setItem("booking_event" + event_id, data.booking_details.event_name);
                window.localStorage.setItem("booking_location" + event_id, data.booking_details.event_location);
                window.localStorage.setItem("booking_number" + event_id, data.booking_details.booking_number);
                window.localStorage.setItem("booking_name" + event_id, data.booking_details.name);

                window.localStorage.setItem("booking_status" + event_id, 'booked');
                window.localStorage.setItem("payment_status" + event_id, 'unpaid');

                load_event_page();
            }
            else
            {
                myApp.hideIndicator();
                myApp.alert('' + data.result + '', 'IOD (Kenya)');
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            myApp.hideIndicator();
            myApp.alert(errorThrown + ' ' + textStatus, 'IoD (Kenya)');
        });
    }

    else
    {
        myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
    }
}

//Pay For Event
$(document).on("click", "button.pay-now", function (e)
{
    myApp.closeModal('.popup-events');
    mainView.router.loadPage('invoice-page.html');
});

//Open event page
function load_event_page()
{
    var event_id = window.localStorage.getItem("current_event_id");

    myApp.popup('.popup-events');
    get_event_details(event_id);
}

function open_client_profile(client_id)
{
    mainView.router.loadPage('client-profile.html');
}



//Login member
$(document).on("submit", "form#add_lead", function (e)
{
    e.preventDefault();

    myApp.showPreloader('Adding Client...');

    var form_data = new FormData(this);

    navigator.geolocation.getCurrentPosition(on_location_client_visit_add, on_location_error);

    service.add_lead(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        window.localStorage.setItem('app_latitude', null);
        window.localStorage.setItem('app_longitude', null);
        if (data.message == "success")
        {
            myApp.closeModal();
            myApp.hidePreloader();

            var personnel_id = window.localStorage.getItem("personnel_id");
            service.get_clients(personnel_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {

                    $("#scheduled-clients-ul").html(data.scheduled_clients);
                    $("#all-clients-ul").html(data.all_clients);
                    $("#leads-ul").html(data.all_leads);


                    myApp.hidePreloader();

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.hidePreloader();
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


        }
        else
        {
            myApp.hidePreloader();
            myApp.alert('' + data.message + '', 'Lead Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hidePreloader();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});



var on_location_client_visit_add = function (position)
{
    app_latitude = position.coords.latitude;
    app_longitude = position.coords.longitude;

    window.localStorage.setItem('app_latitude', app_latitude);
    window.localStorage.setItem('app_longitude', app_longitude);



};

var on_location_opened = function (position)
{
    app_latitude = position.coords.latitude;
    app_longitude = position.coords.longitude;

    window.localStorage.setItem('app_latitude_opened', app_latitude);
    window.localStorage.setItem('app_longitude_opened', app_longitude);



};

function get_client_account(company_id)
{
    window.localStorage.setItem("company_id", company_id);
    mainView.router.loadPage('client-profile.html');


}




function open_stock_div(product_id, current_stock)
{


    service.get_product_detail(product_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            myApp.popup('.popup-stock');

            $("#product-title").html(data.product_name);
            document.getElementById("current-stock").value = current_stock;
            document.getElementById("product_id").value = product_id;
        }
        else
        {
            myApp.alert('Please try again', 'Stock Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
}



$(document).on("submit", "form#add_stock", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    var form_data = new FormData(this);

    service.add_stock_record(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // alert("changes of the close");

            myApp.closeModal();

            mainView.router.loadPage('stock.html');

            var personnel_id = window.localStorage.getItem("personnel_id");
            var company_id = window.localStorage.getItem("company_id");

            service.get_company_stocks(personnel_id, company_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {
                    $("#stock-products").html(data.result);

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


            myApp.hideIndicator();
        }
        else
        {
            myApp.hideIndicator();
            myApp.alert('' + data.message + '', 'Reminder Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});


function open_order_div(product_id, current_stock)
{


    service.get_product_detail(product_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            myApp.popup('.popup-orders');

            $("#order-product-title").html(data.product_name);
            document.getElementById("order-current-stock").value = current_stock;
            document.getElementById("order-product-id").value = product_id;
        }
        else
        {
            myApp.alert('Please try again', 'Stock Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
}

$(document).on("submit", "form#add_order", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    var form_data = new FormData(this);

    service.add_order_record(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // alert("changes of the close");

            myApp.closeModal();

            mainView.router.loadPage('orders.html');

            var personnel_id = window.localStorage.getItem("personnel_id");
            var company_id = window.localStorage.getItem("company_id");
            var order_id = window.localStorage.getItem("order_id");


            service.get_company_orders(personnel_id, company_id, order_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {
                    $("#order-products").html(data.result);

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


            myApp.hideIndicator();
        }
        else
        {
            myApp.hideIndicator();
            myApp.alert('' + data.message + '', 'Reminder Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});




function open_posm_div(posm_id, current_stock)
{

    service.get_posm_detail(posm_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            myApp.popup('.popup-posm');

            $("#posm-title").html(data.posm_name);
            document.getElementById("posm-current-stock").value = current_stock;
            document.getElementById("posm-product-id").value = posm_id;
        }
        else
        {
            myApp.alert('Please try again', 'Stock Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
}

$(document).on("submit", "form#add_sale", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    var form_data = new FormData(this);

    service.add_sale_record(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // alert("changes of the close");

            myApp.closeModal();

            mainView.router.loadPage('sales.html');

            var personnel_id = window.localStorage.getItem("personnel_id");
            var company_id = window.localStorage.getItem("company_id");

            service.get_company_sales(personnel_id, company_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {
                    $("#sale-products").html(data.result);

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


            myApp.hideIndicator();
        }
        else
        {
            myApp.hideIndicator();
            myApp.alert('' + data.message + '', 'Reminder Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});


$(document).on("submit", "form#add_expense", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    var form_data = new FormData(this);

    service.add_expense(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // alert("changes of the close");

            myApp.closeModal();

            var personnel_id = window.localStorage.getItem("personnel_id");

            service.get_expenses(personnel_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {
                    $("#expenses-ul").html(data.result);

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


            myApp.hideIndicator();
        }
        else
        {
            myApp.hideIndicator();
            myApp.alert('' + data.message + '', 'Reminder Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});




function get_clients_view()
{
    var personnel_id = window.localStorage.getItem("personnel_id");
    service.get_clients(personnel_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {

            $("#scheduled-clients-ul").html(data.scheduled_clients);
            $("#all-clients-ul").html(data.all_clients);
            myApp.hideIndicator();

        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });
}



$(document).on("submit", "form#add_schedule", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    var form_data = new FormData(this);

    service.add_schedule(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // alert("changes of the close");
            myApp.hideIndicator();
            myApp.closeModal();

            mainView.router.loadPage('appointments.html');

            get_appointments_view();
        }
        else
        {
            myApp.hideIndicator();
            myApp.alert('' + data.message + '', 'Appontments');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});


function get_appointments_view()
{
    myApp.showPreloader('Loading Appointments');
    var personnel_id = window.localStorage.getItem("personnel_id");
    var company_id = window.localStorage.getItem("company_id");

    service.get_all_schedules(personnel_id, company_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            $("#appointments-ul").html(data.result);

        }
        myApp.hidePreloader();
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.hidePreloader();
                myApp.alert('Please try again', 'Appointments');
            });


    myApp.hidePreloader();
}

function get_leads()
{
    var personnel_id = window.localStorage.getItem("personnel_id");
    service.get_leads(personnel_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {

            $("#leads-ul").html(data.result);
        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });
}


function open_competitor_div(product_id, current_stock)
{


    service.get_competition_product_detail(product_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            myApp.popup('.popup-competition');

            $("#competition-product-title").html(data.product_name);
            document.getElementById("competition-current-stock").value = current_stock;
            document.getElementById("competition_product_id").value = product_id;
        }
        else
        {
            myApp.alert('Please try again', 'SalesPro');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
}

function open_competitor_stock_div(product_id, current_stock)
{


    service.get_competition_product_detail(product_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            myApp.popup('.popup-competition-stock');

            $("#competition-product-title-stock").html(data.product_name);
            document.getElementById("competition-current-stock-product").value = current_stock;
            document.getElementById("competition_product_stock_id").value = product_id;
        }
        else
        {
            myApp.alert('Please try again', 'SalesPro');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Parking ');
    });

    return false;
}

$(document).on("submit", "form#add_competition_price", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    var form_data = new FormData(this);

    service.add_competition_record(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        if (data.message == "success")
        {

            myApp.closeModal();


            var personnel_id = window.localStorage.getItem("personnel_id");

            service.get_competiting_products().done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {

                    $("#products-ul").html(data.result);
                    myApp.hideIndicator();

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });

            myApp.hideIndicator();
        }
        else
        {
            myApp.hideIndicator();
            myApp.alert('' + data.message + '', 'Reminder Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});

$(document).on("submit", "form#add_competition_stock", function (e)
{
    e.preventDefault();
    myApp.showPreloader('Loading products...');

    var form_data = new FormData(this);

    service.add_competition_stock_record(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        if (data.message == "success")
        {
            myApp.hideIndicator();

            myApp.closeModal();

            var personnel_id = window.localStorage.getItem("personnel_id");

            service.get_competiting_stock_products().done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {

                    $("#competitor-stock-products").html(data.result);

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


        }
        else
        {
            myApp.alert('' + data.message + '', 'Reminder Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {

        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });
    myApp.hidePreloader();

    return false;
});

function open_asset_detail_div(asset_id, client_id)
{

    navigator.geolocation.getCurrentPosition(on_location_client_visit_add, on_location_error);

    var app_latitude = window.localStorage.getItem('app_latitude');
    var app_longitude = window.localStorage.getItem('app_longitude');

    if (app_latitude == '' || app_longitude == '')
    {
        myApp.addNotification({
            title: 'Salespro App',
            message: 'Make sure your phone has internet connection and the gps is on'
        });

    }
    else
    {
        service.get_asset_detail(asset_id, client_id, app_latitude, app_longitude).done(function (employees) {
            var data = jQuery.parseJSON(employees);
            // window.localStorage.setItem('app_latitude',null);
            // window.localStorage.setItem('app_longitude',null);
            if (data.message == "success")
            {


                myApp.popup('.popup-asset');
                window.localStorage.setItem('asset_id', asset_id);
                $("#asset-title").html(data.asset_serial_number);
                $("#asset_detail").html(data.result);

            }
            else
            {
                myApp.alert('Please try again', 'Sales Pro');
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            myApp.hideIndicator();
            myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
        });
    }


    return false;
}


function get_promotion_persons(promotion_id)
{

    window.localStorage.setItem('promotion_id', promotion_id);
    service.get_promotion_detail(promotion_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        if (data.message == "success")
        {

            mainView.router.loadPage('promotion-persons.html');

            $("#promotion-title").html(data.promotion_title);

        }
        else
        {
            myApp.alert('Please try again', 'Sales Pro');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
}
function get_promotion_persons_promotion_items(person_id, promotion_id)
{

    service.get_person_promotion_item(person_id, promotion_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        if (data.message == "success")
        {

            // mainView.router.loadPage('promotion-persons-items.html');
            myApp.popup('.popup-promotion-items');

            // $( "#promotion-title" ).html(data.promotion_title);
            $("#promotion-items-ul").html(data.result);

        }
        else
        {
            myApp.alert('Please try again', 'Sales Pro');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;

}

function pick_item(person_id, promotion_id, promotion_item_id)
{

    myApp.modal({
        title: 'Promotion Item',
        text: 'Do you want to proceed to award this item',
        buttons: [
            {
                text: 'Cancel',
                onClick: function () {
                }
            },
            {
                text: 'Award',
                onClick: function () {

                    award_promotion_item(person_id, promotion_id, promotion_item_id);
                }
            },
        ]
    });
}

function award_promotion_item(person_id, promotion_id, promotion_item_id)
{
    service.award_promotion_item(person_id, promotion_id, promotion_item_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        if (data.message == "success")
        {
            myApp.alert('You have successfully awarded the promotion item', 'Sales Pro');

            service.get_person_promotion_item(person_id, promotion_id).done(function (employees) {
                var data = jQuery.parseJSON(employees);
                if (data.message == "success")
                {

                    // mainView.router.loadPage('promotion-persons-items.html');
                    // myApp.popup('.popup-promotion-items');

                    $("#promotion-items-ul").html(data.result);

                }
                else
                {
                    myApp.alert('Please try again', 'Sales Pro');
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {

                myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
            });
        }
        else
        {
            myApp.alert('Please try again', 'Sales Pro');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
}

function unpick_item(person_promotion_item_id, person_id, promotion_id)
{

    myApp.modal({
        title: 'Promotion Item',
        text: 'Do you want to proceed to remove this item',
        buttons: [
            {
                text: 'Cancel',
                onClick: function () {
                }
            },
            {
                text: 'Remove',
                onClick: function () {

                    delete_promotion_item(person_promotion_item_id, person_id, promotion_id);
                }
            },
        ]
    });
}


function delete_promotion_item(person_promotion_item_id, person_id, promotion_id)
{
    service.delete_promotion_item(person_promotion_item_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        if (data.message == "success")
        {
            myApp.alert('You have successfully removed the promotion item', 'Sales Pro');

            service.get_person_promotion_item(person_id, promotion_id).done(function (employees) {
                var data = jQuery.parseJSON(employees);
                if (data.message == "success")
                {

                    // mainView.router.loadPage('promotion-persons-items.html');
                    // myApp.popup('.popup-promotion-items');

                    $("#promotion-items-ul").html(data.result);

                }
                else
                {
                    myApp.alert('Please try again', 'Sales Pro');
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {

                myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
            });
        }
        else
        {
            myApp.alert('Please try again', 'Sales Pro');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
}

$(document).on("submit", "form#add_promotion_person", function (e)
{
    e.preventDefault();

    myApp.showPreloader('Adding Person...');

    var form_data = new FormData(this);

    navigator.geolocation.getCurrentPosition(on_location_client_visit_add, on_location_error);

    service.add_promotion_person(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        window.localStorage.setItem('app_latitude', null);
        window.localStorage.setItem('app_longitude', null);
        if (data.message == "success")
        {
            myApp.closeModal();
            myApp.hidePreloader();

            var personnel_id = window.localStorage.getItem("personnel_id");
            var promotion_id = window.localStorage.getItem('promotion_id');

            service.get_all_persons(personnel_id, promotion_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {

                    $("#persons-ul").html(data.result);
                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });



        }
        else
        {
            myApp.hidePreloader();
            myApp.alert('' + data.message + '', 'Lead Response');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hidePreloader();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});



function open_sale_div(posm_id, current_stock)
{


    service.get_posm_detail(posm_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            myApp.popup('.popup-posm');

            $("#posm-product-title").html(data.product_name);
            document.getElementById("posm-current-stock").value = current_stock;
            document.getElementById("posm-product-id").value = posm_id;
        }
        else
        {
            myApp.alert('Please try again', 'Loyalty App');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
}

$(document).on("submit", "form#add_posm", function (e)
{
    e.preventDefault();
    myApp.showIndicator();

    var form_data = new FormData(this);

    service.add_posm_record(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // alert("changes of the close");

            myApp.closeModal();

            mainView.router.loadPage('posm.html');

            var personnel_id = window.localStorage.getItem("personnel_id");
            var company_id = window.localStorage.getItem("company_id");

            service.get_posm_items(personnel_id, company_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {

                    $("#posm-items").html(data.result);
                    myApp.hideIndicator();

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


            myApp.hideIndicator();
        }
        else
        {
            myApp.hideIndicator();
            myApp.alert('' + data.message + '', 'Loyalty App');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});

function open_questions_page(survey_id)
{

    mainView.router.loadPage('survey-questions.html');

    service.get_survey_questions(survey_id).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {

            $("#survey-title").html(data.survey_title);
            $("#questions-ul").html(data.result);

        }
        else
        {
            myApp.alert('Please try again', 'Loyalty App');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hideIndicator();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;

}


$(document).on("submit", "form#add_survey_answers", function (e)
{
    e.preventDefault();

    myApp.showPreloader('Submitting answer...');

    var form_data = new FormData(this);

    navigator.geolocation.getCurrentPosition(on_location_client_visit_add, on_location_error);

    service.add_survey_answers(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {

            myApp.hidePreloader();
            myApp.alert('Answer Submitted successfully', 'Survey Response');
        }
        else
        {
            myApp.hidePreloader();
            myApp.alert('' + data.message + '', 'Survey Response');
        }
        myApp.hidePreloader();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hidePreloader();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});


function get_orders(personnel_id, company_id, order_id)
{
    myApp.showPreloader('Getting orders ...');
    service.get_company_orders(personnel_id, company_id, order_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {
            $("#order-products").html(data.result);
            $("#order-items-list").html(data.order_items);

        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });
    myApp.hidePreloader();
}

function create_new_order()
{

    myApp.showPreloader('Creating new order ...');

    var personnel_id = window.localStorage.getItem("personnel_id");
    var company_id = window.localStorage.getItem("company_id");

    service.create_new_order(personnel_id, company_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {

            myApp.hidePreloader();
            var order_id = data.order_id;
            var client_order_number = data.client_order_number;
            var order_items = data.order_items;
            window.localStorage.setItem("order_id", order_id);
            window.localStorage.setItem("client_order_number", client_order_number);
            document.getElementById("order_div").style.display = "block";
            document.getElementById("create_order_button").style.display = "none";

            $("#order-number").html(data.client_order_number);
            $("#order-number-submit").html(data.client_order_number);
            $("#order-items-list").html(data.order_items);



            get_orders(personnel_id, company_id, order_id);

        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.hidePreloader();
                myApp.alert('Please try again', 'Salesforce APP');
            });
    myApp.hidePreloader();

}

function get_ordered_items()
{
    var order_id = window.localStorage.getItem("order_id");
    var personnel_id = window.localStorage.getItem("personnel_id");
    var company_id = window.localStorage.getItem("company_id");


    myApp.showPreloader('Getting orders ...');
    service.get_company_orders(personnel_id, company_id, order_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {

            myApp.popup('.popup-order-items');
            $("#order-products").html(data.result);
            $("#order-items-list").html(data.order_items);

        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });
    myApp.hidePreloader();

}

function send_location()
{

    navigator.geolocation.getCurrentPosition(on_location_opened, on_location_error);

    var app_latitude = window.localStorage.getItem('app_latitude_opened');
    var app_longitude = window.localStorage.getItem('app_longitude_opened');


    // if(app_latitude == '' || app_longitude == '')
    // {
    // 	    console.log('not set');
    // }
    // else
    // {
    service.submit_current_location(personnel_id, app_latitude, app_longitude).done(function (employees)
    {

        var data = jQuery.parseJSON(employees);
        window.localStorage.setItem('app_latitude_opened', '');
        window.localStorage.setItem('app_longitude_opened', '');
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });
    // }
}


function get_asset_form()
{


    var personnel_id = window.localStorage.getItem("personnel_id");
    myApp.showPreloader('Getting list ...');
    service.get_every_clients(personnel_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {

            myApp.popup('.popup-add-asset');


            $("#clients-list").html(data.result);
            $("#asset-items").html(data.asset_items);

        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });
    myApp.hidePreloader();

}


$(document).on("submit", "form#add_asset_details", function (e)
{
    e.preventDefault();

    myApp.showPreloader('Adding Asset...');

    var form_data = new FormData(this);

    navigator.geolocation.getCurrentPosition(on_location_client_visit_add, on_location_error);

    service.add_asset_details(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {
            myApp.closeModal();
            myApp.hidePreloader();


            var asset_id = data.asset_id;


            // upload itsms

            var img = document.getElementById('image_asset_new');
            var imageURI = img.src;
            var options = new FileUploadOptions();
            options.fileKey = "upload_image";
            options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            options.mimeType = "image/jpeg";
            var params = new Object();
            options.params = params;
            options.chunkedMode = false;
            var ft = new FileTransfer();

            var app_latitude = window.localStorage.getItem('app_latitude');
            var app_longitude = window.localStorage.getItem('app_longitude');
            var personnel_id = window.localStorage.getItem('personnel_id');


            myApp.showPreloader('Submitting Order ...');
            ft.upload(imageURI, encodeURI(base_url + "client/submit_asset_adding/" + asset_id + "/" + app_latitude + "/" + app_longitude + "/" + personnel_id), asset_add_win, fail,
                    options);
            myApp.hidePreloader();

            myApp.showPreloader('Loading...');
            var personnel_id = window.localStorage.getItem("personnel_id");
            service.get_all_assets(personnel_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {

                    $("#assets-ul").html(data.result);
                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


            myApp.hidePreloader();


            var personnel_id = window.localStorage.getItem("personnel_id");
        }
        else
        {
            myApp.hidePreloader();
            myApp.alert('' + data.message + '', 'Lead Response');
        }


        window.localStorage.setItem('app_latitude', null);
        window.localStorage.setItem('app_longitude', null);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hidePreloader();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});



function get_competitor_form()
{
    var personnel_id = window.localStorage.getItem("personnel_id");
    myApp.showPreloader('Getting list ...');
    service.get_every_competitor(personnel_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {

            myApp.popup('.popup-add-competitors');


            $("#competitor-list").html(data.result);
            $("#product-items").html(data.products);

        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });
    myApp.hidePreloader();

}

function competitor_div(item)
{

    if (item == 1)
    {
        document.getElementById("tabitem1").style.display = "block";
        document.getElementById("tabitem2").style.display = "none";
    }
    else
    {
        document.getElementById("tabitem1").style.display = "none";
        document.getElementById("tabitem2").style.display = "block";
    }

}



$(document).on("submit", "form#add_competitor", function (e)
{
    e.preventDefault();

    myApp.showPreloader('Adding Competitor...');

    var form_data = new FormData(this);

    service.add_competitor_details(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {
            // myApp.closeModal();
            myApp.hidePreloader();
            myApp.alert('You have successfully added a competitor', 'Competitor');



            get_competitor_form();
        }
        else
        {
            myApp.hidePreloader();
            myApp.alert('' + data.message + '', 'Lead Response');
        }


        window.localStorage.setItem('app_latitude', null);
        window.localStorage.setItem('app_longitude', null);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hidePreloader();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});


$(document).on("submit", "form#add_competition", function (e)
{
    e.preventDefault();

    myApp.showPreloader('Adding Competition...');

    var form_data = new FormData(this);

    service.adding_competition(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {
            myApp.closeModal();
            myApp.hidePreloader();
            myApp.alert('You have successfully added a competition', 'Competitor');

            myApp.showPreloader('Loading...');

            var personnel_id = window.localStorage.getItem("personnel_id");

            service.get_competiting_products().done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {

                    $("#products-ul").html(data.result);
                    myApp.hidePreloader();

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


            myApp.hidePreloader();
        }
        else
        {
            myApp.hidePreloader();
            myApp.alert('' + data.message + '', 'Lead Response');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hidePreloader();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});

$(document).on("submit", "form#add_activity", function (e)
{
    e.preventDefault();

    myApp.showPreloader('Adding Activity...');

    var form_data = new FormData(this);

    service.add_activity(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {
            myApp.hidePreloader();
            var competitior_feedback_id = data.competitior_feedback_id;
            competitor_upload(competitior_feedback_id);

        }
        else
        {
            myApp.hidePreloader();
            myApp.alert('' + data.message + '', 'Lead Response');
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hidePreloader();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});


function open_schedule_div(client_id)
{

    // myApp.popup('.popup-visit-schedule');

    var personnel_id = window.localStorage.getItem("personnel_id");
    window.localStorage.setItem("client_id_detail", client_id);
    service.get_client_info(personnel_id, client_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            var company_id = data.company_id;

            myApp.popup('.popup-add-schedule');

            $("#company_visit_name").html(data.company_name);

        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });

    //  myApp.modal({
    //   title:  'VISIT SCHEDULE',
    //   text: 'Do you want to schedule visit today with the client ?',
    //   buttons: [
    //     {
    //       text: 'Yes',
    //       onClick: function() {
    //        	// SCHEDULE A VISIT




    //        	// open 
    //  //       	myApp.showPreloader('Scheduling visit');
    //  //       	var personnel_id = 	window.localStorage.getItem("personnel_id");

    // 	// service.schedule_visit_today(personnel_id,client_id).done(function (employees)
    // 	// {
    // 	// 	var data = jQuery.parseJSON(employees);

    // 	// 	if(data.message == "success")
    // 	// 	{


    // 	// 		myApp.alert('Visit was successfully scheduled','VISIT SCHEDULE');

    // 	// 		mainView.router.loadPage('client.html');

    // 	// 		get_clients_view();

    // 	// 	}
    // 	// 	myApp.hidePreloader();
    // 	// })
    // 	// .fail(function( jqXHR, textStatus, errorThrown) {

    // 	// 	myApp.hidePreloader();
    // 	// 	myApp.alert( 'Please try again', 'Salesforce APP' );
    // 	// });

    //       }
    //     },
    //     {
    //       text: 'No',
    //       onClick: function() {
    //         myApp.alert('Visit was not schedule','VISIT SCHEDULE')
    //       }
    //     },
    //   ]
    // })


}


$(document).on("submit", "form#add_client_schedule", function (e)
{
    e.preventDefault();
    myApp.showPreloader('Scheduling visit');

    var form_data = new FormData(this);

    service.add_client_schedule(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // alert("changes of the close");
            myApp.hidePreloader();
            myApp.closeModal();


            myApp.alert('Visit was successfully scheduled', 'VISIT SCHEDULE');
            myApp.showPreloader('Loading...');
            var personnel_id = window.localStorage.getItem("personnel_id");
            service.get_clients(personnel_id).done(function (employees)
            {
                var data = jQuery.parseJSON(employees);

                if (data.message == "success")
                {

                    $("#scheduled-clients-ul").html(data.scheduled_clients);
                    $("#all-clients-ul").html(data.all_clients);
                    $("#leads-ul").html(data.all_leads);


                    myApp.hidePreloader();

                }
            })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('Please try again', 'Salesforce APP');
                    });


            myApp.hidePreloader();

        }
        else
        {
            myApp.hidePreloader();
            myApp.alert('' + data.message + '', 'Appontments');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hidePreloader();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty App');
    });

    return false;
});





function get_promotion_listings() {
    // body...
    var personnel_id = window.localStorage.getItem("personnel_id");
    service.get_promotion_listings(personnel_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            $("#vehicle-ul-item").html(data.result);
        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });
}

function get_all_zones()
{

    service.get_all_parking_zones().done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.status == "success")
        {
            $(".zones_div").html(data.result);
            $(".lots_div").prop('disabled', true);
            $(".sub_zones_div").prop('disabled', true);
            $("btnQuery").prop('disabled', true);
        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });

}

function get_all_vehicle_types()
{

    service.get_all_vehicle_types().done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.status == "success")
        {
            $(".vehicle_type_div").html(data.result);
        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });

}

$(document).on("change", "select#zones", function (e)
{
    var zone_id = $(this).val();
    // alert(zone_id);
    service.get_zone_parking_lot(zone_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.status == "success")
        {
            $(".lots_div").html(data.result);
            $(".lots_div").prop('disabled', false);
        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });

});
$(document).on("change", "select#parking_lot_id", function (e)
{
    var parking_lot_id = $(this).val();
    // alert(zone_id);
    service.get_sub_zone(parking_lot_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.status == "success")
        {
            $(".sub_zones_div").html(data.result);
            $(".sub_zones_div").prop('disabled', false);
        }
    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });

});


$(document).on("submit", "form#query_vehicle_form", function (e)
{
    e.preventDefault();

    myApp.showPreloader('Querying vehicle...');

    var form_data = new FormData(this);
    service.query_vehicle_parked(form_data).done(function (employees) {
        var data = jQuery.parseJSON(employees);
        myApp.hidePreloader();
        if (data.message == "success")
        {
            myApp.hidePreloader();
            myApp.closeModal();
        }
        else
        {
            myApp.hidePreloader();
            myApp.closeModal();
            myApp.alert('' + data.message + '', 'Loyalty APP');
            
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        myApp.hidePreloader();
        myApp.alert(errorThrown + ' ' + textStatus, 'Loyalty APP');
    });

    return false;
});

function clamp_vehicle(plate_check_id, clamping_id)
{
    myApp.modal({
        title: 'Loyalty APP',
        text: 'Do you want to clamp this vehicle ?',
        buttons: [
            {
                text: 'No',
                onClick: function () {
                }
            },
            {
                text: 'Yes',
                onClick: function () {
                    // SCHEDULE A VISIT
                    // var clamp_number = null;
                    myApp.prompt('Clamp No?', 'Loyalty APP', function (value) {
                        var clamp_number = value;


                        if (clamp_number == null || clamp_number == "")
                        {
                            myApp.alert('Please enter a valid clamp number', 'Loyalty APP');
                        }
                        else
                        {

                            myApp.showPreloader('Clamping Vehicle');
                            var personnel_id = window.localStorage.getItem("personnel_id");

                            service.clamp_vehicle(personnel_id, plate_check_id, clamp_number, clamping_id).done(function (employees)
                            {
                                var data = jQuery.parseJSON(employees);

                                if (data.message == "success")
                                {
                                    get_all_vehicles_listing();
                                    myApp.alert('You have successfully clamped vehicle', 'Loyalty APP');

                                }
                                else
                                {
                                    myApp.alert(data.result, 'Loyalty APP');
                                }
                                myApp.hidePreloader();
                            })
                                    .fail(function (jqXHR, textStatus, errorThrown) {

                                        myApp.hidePreloader();
                                        myApp.alert('Please try again', 'Loyalty APP');
                                    });

                        }
                    });




                }
            },
        ]
    })

}


function declamp_vehicle(plate_check_id, clamping_id)
{
    myApp.modal({
        title: 'Loyalty APP',
        text: 'Do you want to declamp this vehicle ?',
        buttons: [
            {
                text: 'No',
                onClick: function () {
                }
            },
            {
                text: 'Yes',
                onClick: function () {
                    // SCHEDULE A VISIT
                    // var clamp_number = null;
                    myApp.prompt('Clamp No?', 'Loyalty APP', function (value) {
                        var clamp_number = value;


                        if (clamp_number == null || clamp_number == "")
                        {
                            myApp.alert('Please enter a valid clamp number', 'Loyalty APP');
                        }
                        else
                        {

                            myApp.showPreloader('Declamping Vehicle');
                            var personnel_id = window.localStorage.getItem("personnel_id");

                            service.declamp_vehicle(personnel_id, plate_check_id, clamp_number, clamping_id).done(function (employees)
                            {
                                var data = jQuery.parseJSON(employees);

                                if (data.message == "success")
                                {
                                    get_all_vehicles_listing();
                                    myApp.alert('You have successfully declamped vehicle', 'Loyalty APP');

                                }
                                else
                                {
                                    myApp.alert(data.result, 'Loyalty APP');
                                }
                                myApp.hidePreloader();
                            })
                                    .fail(function (jqXHR, textStatus, errorThrown) {

                                        myApp.hidePreloader();
                                        myApp.alert('Please try again', 'Loyalty APP');
                                    });

                        }
                    });




                }
            },
        ]
    })

}

function tow_vehicles(plate_check_id, clamping_id)
{
    myApp.modal({
        title: 'Loyalty APP',
        text: 'Do you want to tow this vehicle ?',
        buttons: [
            {
                text: 'No',
                onClick: function () {
                }
            },
            {
                text: 'Yes',
                onClick: function () {
                    // SCHEDULE A VISIT
                    // var clamp_number = null;
                    myApp.showPreloader('Towing Vehicle');
                    var personnel_id = window.localStorage.getItem("personnel_id");

                    service.tow_vehicle(personnel_id, plate_check_id, clamping_id).done(function (employees)
                    {
                        var data = jQuery.parseJSON(employees);

                        if (data.message == "success")
                        {
                            get_all_vehicles_listing();
                            myApp.alert('You have successfully recorded a tow ', 'Loyalty APP');

                        }
                        else
                        {
                            myApp.alert(data.result, 'Loyalty APP');
                        }
                        myApp.hidePreloader();
                    })
                            .fail(function (jqXHR, textStatus, errorThrown) {

                                myApp.hidePreloader();
                                myApp.alert('Please try again', 'Loyalty APP');
                            });
                }
            },
        ]
    })

}
function get_all_vehicles_listing()
{
    var personnel_id = window.localStorage.getItem("personnel_id");
    service.get_all_vehicle_queries(personnel_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);

        if (data.message == "success")
        {
            // $("#vehicles-ul").html(data.result);
            $("#vehicle-ul-item").html(data.result);
        }

    })
            .fail(function (jqXHR, textStatus, errorThrown) {
                myApp.alert('Please try again', 'Salesforce APP');
            });
}

function logout_function()
{
    window.localStorage.clear();
  

     myApp.modal({
        title:  'LOGOUT',
        text: 'Do you want to log out of the app',
        buttons: [
          {
            text: 'No',
            onClick: function() {
            }
          },
          {
            text: 'Yes',
            onClick: function() {
              
              myApp.closePanel();
              myApp.popup('.login-screen');
            }
          },
        ]
      });
}
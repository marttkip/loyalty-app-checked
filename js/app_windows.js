/* Function to check for network connectivity */
document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is ready
//
function onDeviceReady() 
{
	//back button listener
    document.addEventListener("backbutton", onBackKeyDown, false);
	
	//push notifications
	var push = PushNotification.init({ "android": {"senderID": "540631623027"},
         "ios": {"alert": "true", "badge": "true", "sound": "true"}, "windows": {} } );
	
    push.on('registration', function(data) {
        // data.registrationId
		//console.log(data.registrationId);
		//$("#gcm_id").html(data.registrationId);
		window.localStorage.setItem("iod_registrationId", data.registrationId);
		service.add_registration_id(data.registrationId).done(function (employees) {
			var data2 = jQuery.parseJSON(employees);
			
			console.log(data2);
		});
    });

    push.on('notification', function(data) {
        // data.message,
        // data.title,
        // data.count,
        // data.sound,
        // data.image,
        // data.additionalData
		console.log(data.message);
		//myApp.alert(data.title+" Message: " +data.message, 'Choto');
    });

    push.on('error', function(e) {
        // e.message
		console.log(e.message);
		//myApp.alert(e.message, 'Error');
    });
}

function onBackKeyDown() {
    // Handle the back button
	myApp.showIndicator();
	
	mainView.router.back();
	mainView.router.refreshPage();
	
	myApp.hideIndicator();
}

function onMenuKeyDown() {
	myApp.alert('Hello');
}

$(document).ready(function()
{
	//window.localStorage.clear();
	automatic_login();
});

function automatic_login()
{
	var logged_in = window.localStorage.getItem("iod_logged_in");
	
	if(logged_in == 'yes')
	{
		MSApp.execUnsafeLocalFunction(function () {
			$('a.profile-page span.tabbar-label').html('Profile');
		});
	}
}

$(document).on('pageInit', '.page[data-page="index"]', function (e) 
{
	//mainView.hideNavbar();
})

function HideModalPopup() 
{
	MSApp.execUnsafeLocalFunction(function () {
		$("#ModalBehaviour").hide(); 
	});
}

//Login member
$(document).on("submit","form#login_member",function(e)
{
	e.preventDefault();
	myApp.showIndicator();
	//get form values
	var login_member_no = $('#login_member_no').val();
	var login_member_password = $('#login_member_password').val();
	
	//check if there is a network connection
	var connection = true;//is_connected();
	
	if(connection === true)
	{
		service.login_member(login_member_no, login_member_password).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				window.localStorage.setItem("iod_email_address", data.result.member_email);
				window.localStorage.setItem("iod_password", data.result.password);
				window.localStorage.setItem("iod_logged_in", 'yes');
				window.localStorage.setItem("iod_member_no", data.result.member_no);
				window.localStorage.setItem("iod_member_surname", data.result.member_surname);
				window.localStorage.setItem("iod_member_first_name", data.result.member_first_name);
				window.localStorage.setItem("iod_member_id", data.result.member_id);
				window.localStorage.setItem("iod_first_login", 'no');
				myApp.closeModal();
				myApp.alert('You have successfully logged in', 'IoD (Kenya)');
				
				var iod_login_refferal = window.localStorage.getItem('iod_login_refferal');
				
				if(iod_login_refferal == 'booking')
				{
					book_member(data.result.member_id);
				}
				
				else
				{
					mainView.router.loadPage('profile.html');
				}
				myApp.hideIndicator();
				automatic_login();
			}
			else
			{
				myApp.hideIndicator();
				myApp.alert(''+data.result+'', 'Login Response');
			}
        }).fail(function( jqXHR, textStatus, errorThrown ) {
				myApp.hideIndicator();
				myApp.alert(errorThrown+' '+textStatus, 'IoD (Kenya)');
		});
	}
	
	else
	{
		myApp.alert('No internet connection - please check your internet connection then try again', 'Login Error');
	}
	return false;
});

//Login member
$(document).on("submit","form#change_password",function(e)
{
	e.preventDefault();
	myApp.showIndicator();
	//get form values
	var form_data = new FormData(this);
	
	//check if there is a network connection
	var connection = checkConnection();
	
	//if(connection != 'No network connection')
	if(connection = true)
	{
		service.change_password(form_data).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				myApp.hideIndicator();
				window.localStorage.setItem("first_login", 'no');
				myApp.alert('Password changed successfully', 'IOD (Kenya)');
				
				mainView.router.loadPage('dist/dashboard.html');
	
			}
			else
			{
				myApp.hideIndicator();
				myApp.alert(''+data.result+'', 'IOD (Kenya)');
			}
        }).fail(function( jqXHR, textStatus, errorThrown ) {
				myApp.hideIndicator();
				myApp.alert(errorThrown+' '+textStatus, 'IoD (Kenya)');
		});
	}
	
	else
	{
		myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
	}
	return false;
});
	
//Handler for profile page click
$(document).on("click","a.profile-page",function(e)
{
	e.preventDefault();
	
	var logged_in = window.localStorage.getItem('iod_logged_in');
	//window.localStorage.setItem('iod_logged_in', 'no');
	if(logged_in == "yes")
	{
		mainView.router.loadPage('profile.html');
	}
	
	else
	{
		myApp.loginScreen();
	}
});

function get_profile_details()
{
	$( "#loader-wrapper" ).removeClass( "display_none" );
	
	service.getProfileDetails().done(function (employees) {
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			// $( "#news-of-icpak" ).addClass( "display_block" );
			$( "#my_profile" ).html( data.result );
			$( "#loader-wrapper" ).addClass( "display_none" );
		}
	});
}

function set_news_data()
{
		
		service.getallLatesNews().done(function (employees) {
		
		var data = jQuery.parseJSON(employees);
		
		window.localStorage.setItem("news_history", data.result);
		window.localStorage.setItem("total_news", data.total);
	});
}
function load_messages()
{
	var messages = window.localStorage.getItem("news_history");
	$("#icpak_news").html(messages);
}

function change_to_arms()
{
	get_arms_items();
	window.location.href = "arms.html";
}

//Share post
$(document).on("click","a.share_post",function(e)
{
	e.preventDefault();
	
	var message = $( "#content" ).val();
	var subject = $( "#title" ).val();
	var file = $( "#image" ).val();
	var url = null;
	window.plugins.socialsharing.share(
		  message,
		  subject,
		  file,
		  url,
		  function(result) {/*alert('success: ' + result)*/},
		  function(result) {/*alert('error: ' + result)*/}
	 );
	return false;
});

function display_comments(id)
{
	//get event comments
	var event_comments = window.localStorage.getItem("event_comments"+id);
	//var event_comments = null;
	
	if((event_comments == "") || (event_comments == null) || (event_comments == "null"))
	{
		
		
		service.getNewsComments(id).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				window.localStorage.setItem("event_comments"+id, data.result);
				$( "#forum_comments" ).html( data.result );
			}
		});
	}
	
	else
	{
		$( "#event_comments" ).html( event_comments );
		
		
		service.getNewsComments(id).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				window.localStorage.setItem("event_comments"+id, data.result);
				$( "#forum_comments" ).html( data.result );
			}
		});
	}
}

function refresh_blog_display(id)
{
	var total_blog = window.localStorage.getItem("total_blog"+id);
	var new_blog = window.localStorage.getItem("new_blog"+id);
	var first_load = window.localStorage.getItem("first"+id);
	
	//case of new adds
	if((new_blog != total_blog) && (first_load == 'no'))
	{
		window.localStorage.setItem("total_blog"+id, new_blog);
		var blog_list = window.localStorage.getItem("blog_list"+id);
		$( "#blog_details" ).html( blog_list );
	}
}

function get_blog_description(id)
{
	myApp.showIndicator();
	
	//get event details
	var blog_details = window.localStorage.getItem("blog_details"+id);
	var blog_details = null;
	
	if((blog_details == "") || (blog_details == null) || (blog_details == "null"))
	{
		var member_id = window.localStorage.getItem("iod_member_id");
		
		service.getBlogDetail(id, member_id).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			//alert('here');
			if(data.message == "success")
			{//alert(data.result);
				window.localStorage.setItem("blog_details"+id, data.result);
				window.localStorage.setItem("blog_title"+id, data.discussion_title);
				window.localStorage.setItem("blog_content"+id, data.discussion_content);
				window.localStorage.setItem("forum_post_id", id);
				$( "#blog_details" ).html( data.result );
				$( "h3#forum-title" ).html( data.discussion_title );
				myApp.hideIndicator();
			}
			
			else
			{
				myApp.alert('Oops. Unable to get content', 'ICPAK');
				myApp.hideIndicator();
			}
		});
	}
	
	else
	{
		setTimeout(function () {
			// change the page to home 
			var blog_title = window.localStorage.getItem("blog_title"+id);
			window.localStorage.setItem("post_id", id);
			$( "#blog_details" ).html( blog_details );
			$( "#forum-title" ).html( blog_title );
			myApp.hideIndicator();
		 }, 3000);
	}
	
	var refresh_blog_selection2 = setInterval(function(){ refresh_blog_timer(id) }, 2000);
	var refresh_blog_display2 = setInterval(function(){ refresh_blog_display(id) }, 4000);
}

function refresh_blog_timer(id)
{
	var member_id = window.localStorage.getItem("iod_member_id");
	
	service.getBlogDetail(id, member_id).done(function (employees) {
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			var total_blog = window.localStorage.getItem("total_blog"+id);
			var new_blog = data.total_received;
			window.localStorage.setItem("new_blog"+id, new_blog);
			
			if(total_blog != data.total_received)
			{
				window.localStorage.setItem("blog_list"+id, data.result);
			}
		}
	});
}

function refresh_blog_display(id)
{
	var total_blog = window.localStorage.getItem("total_blog"+id);
	var new_blog = window.localStorage.getItem("new_blog"+id);
	
	//case of new adds
	if(new_blog != total_blog)
	{
		window.localStorage.setItem("total_blog"+id, new_blog);
		var blog_list = window.localStorage.getItem("blog_list"+id);
		$( "#blog_details" ).html( blog_list );
	}
}

//Login member
$(document).on("click","a.post-notification",function(e)
{
	var post_id = window.localStorage.getItem("forum_post_id");
	var blog_title = window.localStorage.getItem("blog_title"+post_id);
	var blog_content = window.localStorage.getItem("blog_content"+post_id);
	myApp.addNotification({
		title: blog_title,
		message: blog_content
	});	
});

//Login member
$(document).on("click","a.add-comment",function()
{
	myApp.showIndicator();
	
	//check if there is a network connection
	var connection = checkConnection();
	
	//if(connection != 'No network connection')
	if(connection = true)
	{
		var member_id = window.localStorage.getItem("iod_member_id");
		var comment = $("#post_comment").val();
		var post_id = window.localStorage.getItem("forum_post_id");
		service.add_comment(comment, post_id, member_id).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				myApp.hideIndicator();
				$("#post_comment").val('');
				get_blog_description(post_id);
			}
			else
			{
				myApp.hideIndicator();
				myApp.alert(data.result, 'Error');
			}
        }).fail(function( jqXHR, textStatus, errorThrown ) {
				myApp.hideIndicator();
				myApp.alert(errorThrown+' '+textStatus, 'IoD (Kenya)');
		});
	}
	
	else
	{
		myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
		myApp.hideIndicator();
	}
	return false;
});

//Login member
$(document).on("submit","form#CommentForm",function(e)
{
	e.preventDefault();
	myApp.showIndicator();
	
	//check if there is a network connection
	var connection = checkConnection();
	
	//if(connection != 'No network connection')
	if(connection = true)
	{
		
		
		var member_id = window.localStorage.getItem("iod_member_id");
		var comment = $("textarea[name=comment]").val();
		var post_id = $("input[name=post_id]").val();
		service.add_comment(comment, post_id, member_id).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				myApp.hideIndicator();
				display_comments(post_id);
			}
			else
			{
				myApp.hideIndicator();
				myApp.alert(data.result, 'Error');
			}
        }).fail(function( jqXHR, textStatus, errorThrown ) {
				myApp.hideIndicator();
				myApp.alert(errorThrown+' '+textStatus, 'IoD (Kenya)');
		});
	}
	
	else
	{
		myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
		myApp.hideIndicator();
	}
	return false;
});

function refresh_forum_timer()
{
	var member_type_id = window.localStorage.getItem("member_type_id");
	
	service.get_forum_items(member_type_id).done(function (employees) {
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			var total_forum = window.localStorage.getItem("total_forum");
			var new_forum = data.total_received;
			window.localStorage.setItem("new_forum", new_forum);
			
			if(total_forum != data.total_received)
			{
				window.localStorage.setItem("forum_list", data.result);
			}
		}
		
		else
		{
			myApp.hideIndicator();
			// myApp.alert("Please check your membership type then try again", 'Error');
			myApp.alert("Please log in as a member to access the forums", 'Error');
			
			
			mainView.router.loadPage('dist/dashboard.html');
		}
	});
}

function refresh_forum_display()
{
	var total_forum = window.localStorage.getItem("total_forum");
	var new_forum = window.localStorage.getItem("new_forum");
	var forum_list = window.localStorage.getItem("forum_list");
	
	if(forum_list == " ")
	{
		new_forum = total_forum;
		window.localStorage.setItem("total_forum", new_forum);
	}
	
	//case of new adds
	if(new_forum != total_forum)
	{
		window.localStorage.setItem("total_forum", new_forum);
		$( "#all_forums" ).html( forum_list );
	}
}

$(document).on("submit","form#ContactFormHe",function(e)
{
	e.preventDefault();
	myApp.showIndicator();
	
	//get form values
	var form_data = new FormData(this);

	//check if there is a network connection
	var connection = checkConnection();
	
	//if(connection != 'No network connection')
	if(connection = true)
	{
		
		service.post_contact_us(form_data).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				myApp.hideIndicator();
				myApp.alert("Thank you for contact us, we will address your issue and get back to you shortly.", 'IOD (Kenya)');
		
			}
			else
			{
				myApp.hideIndicator();
				myApp.alert(data.result, 'IOD (Kenya)');
			}
        });
	}
	
	else
	{
		myApp.hideIndicator();
		myApp.alert("No internet connection - please check your internet connection then try again", 'IOD (Kenya)');
	}
	return false;
});

//pass the variable in the link as follows e.g. news.html?id=1
//on the news.html page get the parameter by javascript as follows var id = getURLParameter('id');
//the function to get the url parameter is defined below
function getURLParameter(name)
{
	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}


function refresh_initiatives_timer()
{
	service.getallinitiatives().done(function (employees) {
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			var total_initiatives = window.localStorage.getItem("total_initiatives");
			var new_initiatives = data.total_received;
			window.localStorage.setItem("new_initiatives", new_initiatives);
			
			if(total_initiatives != data.total_received)
			{
				window.localStorage.setItem("initiatives_list", data.result);
			}
		}
	});
}

function refresh_initiatives_display()
{
	var total_initiatives = window.localStorage.getItem("total_initiatives");
	var new_initiatives = window.localStorage.getItem("new_initiatives");
	
	//case of new adds
	if(new_initiatives != total_initiatives)
	{
		window.localStorage.setItem("total_initiatives", new_initiatives);
		var initiatives_list = window.localStorage.getItem("initiatives_list");
		$( "#all_initiatives" ).html( initiatives_list );
	}
}

function get_initiative_page(id)
{
	myApp.showIndicator();
	//var sermon_details = null;
	var initiative_items = window.localStorage.getItem("initiative_items"+id);
	//initiative_items = null;
	if((initiative_items == "") || (initiative_items == null) || (initiative_items == "null"))
	{
		
		
		service.getInitativePage(id).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				window.localStorage.setItem("initiative_items"+id, data.result);
				$( "#initiative_items" ).html( data.result );
				myApp.hideIndicator();
			}
			
			else
			{
				myApp.hideIndicator();
				myApp.alert('Oops. Unable to get initiatives', 'IOD (Kenya)');
			}
		});
	}
	
	else
	{
		setTimeout(function () {
			// change the page to home 
			$( "#initiative_items" ).html( initiative_items );
			myApp.hideIndicator();
		 }, 3000);
	}
}

function get_initiatives_description(id, parent_id)
{
	myApp.showIndicator();
	//var sermon_details = null;
	var initiative_details = window.localStorage.getItem("initiative_details"+id);
	
	if((initiative_details == "") || (initiative_details == null) || (initiative_details == "null"))
	{
		
		
		service.getInitiativeDetail(id,parent_id).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				window.localStorage.setItem("initiative_details"+id, data.result);
				$( "#initiatives_detail" ).html( data.result );
				myApp.hideIndicator();
			}
			
			else
			{
				myApp.alert('Oops. Unable to get initiative', 'IOD (Kenya)');
				myApp.hideIndicator();
			}
		});
	}
	
	else
	{
		setTimeout(function () {
			// change the page to home 
			$( "#initiatives_detail" ).html( initiative_details );
			myApp.hideIndicator();
		 }, 3000);
	}
}

var myPhotoBrowserads = myApp.photoBrowser({
	photos : [
		'images/long-logo.png',
		'images/advertise-here.png',
		'images/advert-01.png',
	]
});
//Open Home Slider photo browser on click
$$('.pb-slideshow').on('click', function () {
	myPhotoBrowserads.open();
});

$(document).on("click","a.download-resource",function(e)
{
	e.preventDefault();
	myApp.showIndicator();
	var download_file = $(this).attr('download_file');
	//myApp.alert(download_file);
	//var ref = cordova.InAppBrowser.open('http://apache.org', '_blank', 'location=yes');
	window.open(download_file, '_system');
	myApp.hideIndicator();
	/*var fileTransfer = new FileTransfer();
	var uri = encodeURI(download_file);
	//var fileURL = cordova.file.dataDirectory;
	var fileURL =  "cdvfile://localhost/persistent/path/to/file.pdf";
	//myApp.alert(fileURL);
	
	fileTransfer.download(
		uri,
		fileURL,
		function(entry) {
			//myApp.alert("download complete: " + entry.toURL());
			var ref = cordova.InAppBrowser.open(entry.toURL(), '_blank', 'location=no,closebuttoncaption=Close,enableViewportScale=yes');
			//window.open(entry.toURL(), '_blank', 'location=no,closebuttoncaption=Close,enableViewportScale=yes');
			myApp.hideIndicator();
		},
		function(error) {
			myApp.alert("download error source " + error.source+" download error target " + error.target+" upload error code" + error.code);
			myApp.hideIndicator();
		},
		false,
		{
			headers: {
				"Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
			}
		}
	);*/
	
	return false;
});

function get_single_post(id)
{
	myApp.showIndicator();

	var blog_items = window.localStorage.getItem("blog_item"+id);
	$( "#blog_item" ).html( blog_items );
	
	service.get_blog_item(id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			$( "#blog_item" ).html( data.result );
			window.localStorage.setItem("blog_item"+id, data.result);
		}
	});
	
	myApp.hideIndicator();
}

function get_event_details(id)
{
	myApp.showIndicator();
	//var sermon_details = null;
	window.localStorage.setItem("current_event_id", id);
	var event_details = window.localStorage.getItem("event_details"+id);
			
	var event_type_name = window.localStorage.getItem("event_type_name"+id);
	var event_name = window.localStorage.getItem("event_name"+id);
	var start_date = window.localStorage.getItem("start_date"+id);
	var end_date = window.localStorage.getItem("end_date"+id);
	var event_description = window.localStorage.getItem("event_description"+id);
	var event_timetable = window.localStorage.getItem("event_timetable"+id);
	var event_venue = window.localStorage.getItem("event_venue"+id);
	var event_latitude = window.localStorage.getItem("event_latitude"+id);
	var event_longitude = window.localStorage.getItem("event_longitude"+id);
	var event_time = window.localStorage.getItem("event_time"+id);
	
	if(event_name !== 'undefined')
	{	
		$( "#event_type_name" ).html( event_type_name );
		$( "#event_name" ).html( event_name );
		$( "#start_date" ).html( start_date );
		$( "#end_date" ).html( end_date );
		$( "#event_description" ).html( event_description );
		$( "#event_timetable" ).html( event_timetable );
		$( "#event_venue" ).html( event_venue );
		$( "#event_time" ).html( event_time );
	}
	
	//generate_map(event_latitude, event_longitude);
	
	service.get_event_details(id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			event_type_name = data.result.event_type_name;
			event_name = data.result.event_name;
			event_web_name = data.result.event_web_name;
			start_date = data.result.start_date;
			end_date = data.result.end_date;
			event_description = data.result.event_description;
			event_timetable = data.result.event_timetable;
			event_venue = data.result.event_venue;
			event_latitude = data.result.event_latitude;
			event_longitude = data.result.event_longitude;
			event_time = data.result.event_time;
			
			window.localStorage.setItem("event_type_name"+id, event_type_name);
			window.localStorage.setItem("event_name"+id, event_name);
			window.localStorage.setItem("start_date"+id, start_date);
			window.localStorage.setItem("end_date"+id, end_date);
			window.localStorage.setItem("event_description"+id, event_description);
			window.localStorage.setItem("event_timetable"+id, event_timetable);
			window.localStorage.setItem("event_venue"+id, event_venue);
			window.localStorage.setItem("event_latitude"+id, event_latitude);
			window.localStorage.setItem("event_longitude"+id, event_longitude);
			window.localStorage.setItem("event_time"+id, event_time);
			
			$( "#event_type_name" ).html( event_type_name );
			$( "#event_name" ).html( event_name );
			$( "#start_date" ).html( start_date );
			$( "#end_date" ).html( end_date );
			$( "#event_description" ).html( event_description );
			$( "#event_timetable" ).html( event_timetable );
			$( "#event_venue" ).html( event_venue );
			$( "#event_latitude" ).html( event_latitude );
			$( "#event_longitude" ).html( event_longitude );
			$( "#event_time" ).html( event_time );
			
			//remove book now button if already booked
			var booking_status = window.localStorage.getItem("booking_status"+id);
			
			if(booking_status == 'booked')
			{
				$( "button.book-now" ).css( 'display', 'none' );
				var payment_status = window.localStorage.getItem("payment_status"+id);
				
				if(payment_status == 'unpaid')
				{
					$( "button.pay-now" ).css( 'display', 'block' );
				}
			}
			
			generate_map(event_latitude, event_longitude);
			
			myApp.hideIndicator();
		}
		
		else
		{
			myApp.alert('Oops. Unable to get details', 'IOD (Kenya)');
			myApp.hideIndicator();
		}
	});
}

function generate_map(event_latitude, event_longitude)
{
	if((event_longitude > 0) || (event_longitude < 0))
	{
		var my_latitude = event_latitude;
		var my_longitude = event_longitude;
		
		bounds = new google.maps.LatLngBounds();
		myLatlng = new google.maps.LatLng(my_latitude, my_longitude);
		var mapOptions = {
			zoom: 15,
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true
		};
		
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		
		//alert(event_longitude);
		var customer_marker = {
			url: 'images/customer-marker.png',
			// size: new google.maps.Size(63, 56),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(20, 64)
		};
		
		myLatlng = new google.maps.LatLng(my_latitude, my_longitude);
		
		map_center_marker = new google.maps.Marker({
				icon : customer_marker,
				position: myLatlng,
				map: map,
				animation: google.maps.Animation.DROP,
				title: 'Me'
			});	
	}
}

//Book Event
$(document).on("click","button.book-now",function(e)
{
	myApp.showIndicator();
	window.localStorage.setItem('iod_logged_in', 'no');
	var logged_in = window.localStorage.getItem('iod_logged_in');
	
	myApp.closeModal('.popup-events');
	if(logged_in == "yes")
	{
		var member_id = window.localStorage.getItem("iod_member_id");
		book_member(member_id);
		myApp.hideIndicator();
	}
	
	else
	{
		window.localStorage.setItem('iod_login_refferal', 'booking');
		mainView.router.loadPage('book.html');
		myApp.hideIndicator();
	}
});

//Login member
$(document).on("submit","form#book-non-member",function(e)
{
	e.preventDefault();
	myApp.showIndicator();
	var form_data = new FormData(this);
	
	//check if there is a network connection
	var connection = true;//checkConnection();
	
	//if(connection != 'No network connection')
	if(connection = true)
	{
		service.book_non_member(form_data).done(function (employees) {
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				myApp.hideIndicator();
				myApp.closeModal('.popup-non-member');
				myApp.alert('Booking successfully', 'IOD (Kenya)');
				
				var event_id = window.localStorage.getItem("current_event_id");
				
				window.localStorage.setItem("booking_id"+event_id, data.booking_id);
				window.localStorage.setItem("booking_cost"+event_id, data.booking_details.booking_cost);
				window.localStorage.setItem("booking_date"+event_id, data.booking_details.event_start_time);
				window.localStorage.setItem("booking_event"+event_id, data.booking_details.event_name);
				window.localStorage.setItem("booking_location"+event_id, data.booking_details.event_location);
				window.localStorage.setItem("booking_number"+event_id, data.booking_details.booking_number);
				window.localStorage.setItem("booking_name"+event_id, data.booking_details.name);
				
				window.localStorage.setItem("booking_status"+event_id, 'booked');
				window.localStorage.setItem("payment_status"+event_id, 'unpaid');
				
				load_event_page();
			}
			else
			{
				myApp.hideIndicator();
				myApp.alert(''+data.result+'', 'IOD (Kenya)');
			}
        });
	}
	
	else
	{
		myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
	}
	return false;
});

function book_member(member_id)
{
	myApp.showIndicator();
	//check if there is a network connection
	var connection = true;//checkConnection();
	
	//if(connection != 'No network connection')
	if(connection = true)
	{
		service.book_member(member_id).done(function (employees) 
		{
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				myApp.hideIndicator();
				
				myApp.alert('Booking successfully', 'IOD (Kenya)');
				
				var event_id = window.localStorage.getItem("current_event_id");
				
				window.localStorage.setItem("booking_id"+event_id, data.booking_id);
				window.localStorage.setItem("booking_cost"+event_id, data.booking_details.booking_cost);
				window.localStorage.setItem("booking_date"+event_id, data.booking_details.event_start_time);
				window.localStorage.setItem("booking_event"+event_id, data.booking_details.event_name);
				window.localStorage.setItem("booking_location"+event_id, data.booking_details.event_location);
				window.localStorage.setItem("booking_number"+event_id, data.booking_details.booking_number);
				window.localStorage.setItem("booking_name"+event_id, data.booking_details.name);
				
				window.localStorage.setItem("booking_status"+event_id, 'booked');
				window.localStorage.setItem("payment_status"+event_id, 'unpaid');
				
				load_event_page();
			}
			else
			{
				myApp.hideIndicator();
				myApp.alert(''+data.result+'', 'IOD (Kenya)');
			}
        }).fail(function( jqXHR, textStatus, errorThrown ) {
				myApp.hideIndicator();
				myApp.alert(errorThrown+' '+textStatus, 'IoD (Kenya)');
		});
	}
	
	else
	{
		myApp.alert('No internet connection - please check your internet connection then try again', 'IOD (Kenya)');
	}
}

//Pay For Event
$(document).on("click","button.pay-now",function(e)
{
	myApp.closeModal('.popup-events');
	mainView.router.loadPage('invoice-page.html');
});

//Open event page
function load_event_page()
{
	var event_id = window.localStorage.getItem("current_event_id");
	
	myApp.popup('.popup-events');
	get_event_details(event_id);
}
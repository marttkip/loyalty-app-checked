$(document).on('pageInit', '.page[data-page="crimes-page"]', function (e) 
{
	myApp.closePanel();
	
	// alert("changes");
	myApp.showPreloader('Loading...');
	var personnel_id = 	window.localStorage.getItem("personnel_id");
	service.get_customer_details(personnel_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{


			$( "#customer_name" ).html( data.customer_name );
			$( "#customer_phone" ).html( data.customer_phone );
			$( "#card_number" ).html( data.card_number );
			$( "#stock" ).html( data.points_available );
			$( "#orders" ).html( data.points_redeemed );
			$( "#last-updated" ).html ( data.last_updated );
			$( "#last-stock-update" ).html ( data.last_updated );

		}

	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});

	myApp.hidePreloader();
})
$(document).on('pageInit', '.page[data-page="index-page"]', function (e) 
{
	// myApp.closePanel();
	get_promotion_listings();
})


$(document).on('pageInit', '.page[data-page="transactions-page"]', function (e) 
{
	myApp.closePanel();
	// alert("changes");
	myApp.showPreloader('Loading...');
	 var personnel_id = window.localStorage.getItem("personnel_id");
	service.get_all_product_purchases(personnel_id).done(function (employees)
    {
        var data = jQuery.parseJSON(employees);
        if (data.message == "success")
        {
            $("#transactions-ul").html(data.result);
        }
		myApp.hidePreloader();
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        myApp.alert('Please try again', 'Salesforce APP');
    });
		
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="profile-account"]', function (e) 
{
	
	myApp.showPreloader('Loading...');
	var personnel_id = 	window.localStorage.getItem("personnel_id");
	service.get_profile(personnel_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.status == "success")
		{



			$( "#personnel_name" ).html( data.message.personnel_fname+' '+data.message.personnel_onames );
			$( "#personnel_email" ).html( data.message.personnel_email );
			$( "#personnel_phone" ).html( data.message.personnel_phone );
			$( "#account_status" ).html( data.message.account_status );


			document.getElementById("personnel_email_input").value = data.message.personnel_email;
			document.getElementById("personnel_phone_input").value = data.message.personnel_phone;



			var personnel_id = 	window.localStorage.getItem("personnel_id");

			service.get_expenses(personnel_id).done(function (employees)
			{
				var data = jQuery.parseJSON(employees);
				
				if(data.message == "success")
				{
					$( "#expenses-ul" ).html( data.result );

				}
			})
			.fail(function( jqXHR, textStatus, errorThrown) {
				// myApp.alert( 'Please try again', 'Salesforce APP' );
			});



		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="reminders-page"]', function (e) 
{
	myApp.showPreloader('Loading...');
	var personnel_id = 	window.localStorage.getItem("personnel_id");
	service.get_reminders(personnel_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#reminders-ul" ).html( data.result );
		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="asset-page"]', function (e) 
{
	myApp.showPreloader('Loading...');
	var personnel_id = 	window.localStorage.getItem("personnel_id");
	service.get_all_assets(personnel_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#assets-ul" ).html( data.result );
		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="promotions-page"]', function (e) 
{
	myApp.showPreloader('Loading...');
	var personnel_id = 	window.localStorage.getItem("personnel_id");
	service.get_all_promotions(personnel_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#promotions-ul" ).html( data.result );
		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="persons-page"]', function (e) 
{
	myApp.showPreloader('Loading...');
	
	var personnel_id = 	window.localStorage.getItem("personnel_id");
	var promotion_id = window.localStorage.getItem('promotion_id');

	service.get_all_persons(personnel_id,promotion_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#persons-ul" ).html( data.result );
		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})




$(document).on('pageInit', '.page[data-page="leads-page"]', function (e) 
{
	myApp.showPreloader('Loading...');
	var personnel_id = 	window.localStorage.getItem("personnel_id");
	service.get_leads(personnel_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#leads-ul" ).html( data.result );
		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="clients-page"]', function (e) 
{
	myApp.showPreloader('Loading...');
	var personnel_id = 	window.localStorage.getItem("personnel_id");
	service.get_clients(personnel_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#scheduled-clients-ul" ).html( data.scheduled_clients );
			$( "#all-clients-ul" ).html( data.all_clients );
			$( "#leads-ul" ).html( data.all_leads );


			myApp.hidePreloader();

		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})


$(document).on('pageInit', '.page[data-page="stock-page"]', function (e) 
{
	myApp.showPreloader('Loading...');

	var personnel_id = 	window.localStorage.getItem("personnel_id");
	var company_id = 	window.localStorage.getItem("company_id");

	service.get_company_stocks(personnel_id,company_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#stock-products" ).html( data.result );
			myApp.hidePreloader();

		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})


$(document).on('pageInit', '.page[data-page="competitor-stock-page"]', function (e) 
{
	myApp.showPreloader('Loading...');

	var personnel_id = 	window.localStorage.getItem("personnel_id");

	service.get_competiting_stock_products().done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#competitor-stock-products" ).html( data.result );
			myApp.hidePreloader();

		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})


$(document).on('pageInit', '.page[data-page="orders-page"]', function (e) 
{
	myApp.showPreloader('Loading...');

	// check if there is a current active order for the client and the ersonnel
	var personnel_id = 	window.localStorage.getItem("personnel_id");
	var company_id = 	window.localStorage.getItem("company_id");
	service.check_active_order(personnel_id,company_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		if(data.message == "success")
		{
			var order_id  = data.order_id;
			var client_order_number  = data.client_order_number;
			var order_items  = data.order_items;
			window.localStorage.setItem("order_id",order_id);
			window.localStorage.setItem("client_order_number",client_order_number);
			document.getElementById( "order_div" ).style.display = "block";

			$( "#order-number" ).html( data.client_order_number );
			$( "#order-number-submit" ).html( data.client_order_number );
			$( "#order-items-list" ).html( data.order_items );

			get_orders(personnel_id,company_id,order_id);
			

		}
		else
		{


			 myApp.modal({
					    title:  'Order Request',
					    text: 'Do you want to create an order ?',
					    buttons: [
					      {
					        text: 'No',
					        onClick: function() {
					          	document.getElementById( "create_order_button" ).style.display = "block";
					        }
					      },
					      {
					        text: 'Yes',
					        onClick: function() {
					           create_new_order();
					        }
					      }
					    ]
					  })

		}

	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});

	
	
	
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="sales-page"]', function (e) 
{
	myApp.showPreloader('Loading...');

	var personnel_id = 	window.localStorage.getItem("personnel_id");
	var company_id = 	window.localStorage.getItem("company_id");

	service.get_company_sales(personnel_id,company_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#sale-products" ).html( data.result );
			myApp.hidePreloader();

		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="client-profile"]', function (e) 
{
	myApp.showPreloader('Loading client\'s profile...');

	var personnel_id = 	window.localStorage.getItem("personnel_id");
	var company_id = 	window.localStorage.getItem("company_id");



	service.get_client_profile(personnel_id,company_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			var company_id = data.company_id;

			window.localStorage.setItem("company_id",data.company_id);
			window.localStorage.setItem("company_name",data.company_name);
			window.localStorage.setItem("company_contact_person_name",data.company_contact_person_name);
			window.localStorage.setItem("company_contact_person_phone",data.company_contact_person_phone);
			window.localStorage.setItem("company_contact_person_email",data.company_contact_person_email);

			$( "#company_name" ).html( data.company_name );
			$( "#company_contact_person_name" ).html( data.company_contact_person_name );
			$( "#company_contact_person_phone" ).html( data.company_contact_person_phone );
			$( "#company_contact_person_email" ).html( data.company_contact_person_email );
			$( "#stock" ).html( data.stock );
			$( "#appointments" ).html( data.appointments );
			$( "#orders" ).html( data.orders );
			$( "#sales" ).html( data.sales );

			get_client_navigation_status();

			var client_active  = window.localStorage.getItem("client_active");
			
			if(client_active == company_id)
			{
				document.getElementById( "error_location" ).style.display = "none";
				document.getElementById( "close_visit" ).style.display = "block";
				document.getElementById( "bottomid" ).style.display = "block";
			}
			else
			{
				// window.localStorage.setItem("client_active",'');

				document.getElementById( "bottomid" ).style.display = "none";
				document.getElementById( "error_location" ).style.display = "block";
				document.getElementById( "close_visit" ).style.display = "none";
			}

			

			myApp.hidePreloader();
			
		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})



$(document).on('pageInit', '.page[data-page="map-page"]', function (e) 
{
	myApp.showPreloader('Loading...');

	initMap();
	myApp.hidePreloader();
})
$(document).on('pageInit', '.page[data-page="calendar-page"]', function (e) 
{
	
})

$(document).on('pageInit', '.page[data-page="appointments-page"]', function (e) 
{
	get_appointments_view();
})




$(document).on('pageInit', '.page[data-page="competitions-page"]', function (e) 
{
	myApp.showPreloader('Loading...');

	var personnel_id = 	window.localStorage.getItem("personnel_id");

	service.get_competiting_products().done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#products-ul" ).html( data.result );
			myApp.hidePreloader();

		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="posm-page"]', function (e) 
{
	myApp.showPreloader('Loading...');

	var personnel_id = 	window.localStorage.getItem("personnel_id");
	var company_id = 	window.localStorage.getItem("company_id");

	service.get_posm_items(personnel_id,company_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#posm-items" ).html( data.result );
			myApp.hidePreloader();

		}
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
	});
	
	
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="survey-page"]', function (e) 
{
	myApp.showPreloader('Loading surveys...');

	var personnel_id = 	window.localStorage.getItem("personnel_id");

	service.get_survey(personnel_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#survey-ul" ).html( data.result );
		

		}
		myApp.hidePreloader();
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
		myApp.hidePreloader();
	});
	
	
	myApp.hidePreloader();
})

$(document).on('pageInit', '.page[data-page="uploads-page"]', function (e) 
{
	myApp.showPreloader('Loading...');

	var personnel_id = 	window.localStorage.getItem("personnel_id");
	var company_id = 	window.localStorage.getItem("company_id");

	service.get_feedback_details(personnel_id,company_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{

			$( "#feedback_item" ).html( data.result );
		

		}
		myApp.hidePreloader();
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
		myApp.hidePreloader();
	});
	myApp.hidePreloader();
})
$(document).on('pageInit', '.page[data-page="activity-page"]', function (e) 
{
	myApp.showPreloader('Loading...');

	var personnel_id = 	window.localStorage.getItem("personnel_id");

	service.get_feedback_items(personnel_id).done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			$( "#competitor_feedback_item" ).html( data.result );
		}
		myApp.hidePreloader();
	})
	.fail(function( jqXHR, textStatus, errorThrown) {
		myApp.alert( 'Please try again', 'Salesforce APP' );
		myApp.hidePreloader();
	});
	myApp.hidePreloader();
})

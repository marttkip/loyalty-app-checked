$$(document).on('pageInit', '.page[data-page="login-screen"]', function (e) 
{
	//mainView.hideNavbar();
	
	myApp.hideIndicator();
	
	/*var logged_in = window.localStorage.getItem("logged_in");
	
	if(logged_in == 'yes')
	{
		var first_login = window.localStorage.getItem("first_login");
		
		if(first_login == 'yes')
		{
			mainView.router.loadPage('dist/change_password.html');
		}
		
		else
		{
			mainView.router.loadPage('dist/dashboard.html');
		}
	}*/
})

$(document).on('pageInit', '.page[data-page="chat"]', function (e) 
{
	myApp.showIndicator();
	var member_id = window.localStorage.getItem("iod_member_id");
	$("#chat_member_id").val(member_id);
	var member_name =  window.localStorage.getItem("iod_member_first_name");

	//window.localStorage.setItem("logged_in", 'no');
	var logged_in = window.localStorage.getItem("iod_logged_in");
	//alert(logged_in);
	if(logged_in == 'yes')
	{
		var member_id = window.localStorage.getItem("iod_member_id");
		$("#chat_member_id").val(member_id);
			
		var forum_list = window.localStorage.getItem("forum_list");
		//forum_list = null;
		
		if((forum_list == "") || (forum_list == null) || (forum_list == "null"))
		{
			service.get_forum_items().done(function (employees)
			{
				var data = jQuery.parseJSON(employees);
				
				if(data.message == "success")
				{
					$( "#all_forums" ).html( data.result );
					window.localStorage.setItem("forum_list", data.result);
					window.localStorage.setItem("total_forum", data.total_received);
				}
				
				else
				{
				
					myApp.alert(data.result, 'Error');
				}
			});
		}
		
		else
		{
			$( "#all_forums" ).html( forum_list );
		}
		
		refresh_ads_selection = setInterval(function(){ refresh_forum_timer() }, 20000);
		refresh_ads_display = setInterval(function(){ refresh_forum_display() }, 30000);
		
		// get_messages();
		// get_contacts();
	}
	
	else
	{
		myApp.hideIndicator();
		myApp.modalLogin('Please log in to join the forum', 'Login', login_member);
	}
	myApp.hideIndicator();
})

$(document).on('pageInit', '.page[data-page="invoices"]', function (e) 
{
	myApp.showIndicator();

	var member_id = window.localStorage.getItem("iod_member_id");

	//window.localStorage.setItem("logged_in", 'no');
	var logged_in = window.localStorage.getItem("iod_logged_in");
	//alert(logged_in);
	if(logged_in == 'yes')
	{
		var previous_invoices = window.localStorage.getItem("previous_invoices");
		$( "#all_invoices" ).html( previous_invoices );
		
		service.get_member_invoices(member_id).done(function (employees)
		{
			var data = jQuery.parseJSON(employees);
			
			if(data.message == "success")
			{
				$( "#all_invoices" ).html( data.result );
				window.localStorage.setItem("previous_invoices", data.result);
			}
			
			else
			{
			
				myApp.alert(data.result, 'IoD (Kenya)');
			}
		});
	}
	
	else
	{
		myApp.alert('Please log in to view your invoices', 'IoD (Kenya)');
	}
	myApp.hideIndicator();
})

$(document).on('pageInit', '.page[data-page="calendar"]', function (e) 
{
	myApp.showIndicator();

	var calendar_items = window.localStorage.getItem("calendar_items");
	$( "#calendar_items" ).html( calendar_items );
	
	service.get_calendar_items().done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			$( "#calendar_items" ).html( data.result );
			window.localStorage.setItem("calendar_items", data.result);
		}
	});
	
	myApp.hideIndicator();
})

$(document).on('pageInit', '.page[data-page="resources"]', function (e) 
{
	myApp.showIndicator();

	var resources = window.localStorage.getItem("resources");
	$( "#resources-list" ).html( resources );
	
	service.get_resources().done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			$( "#resources-list" ).html( data.result );
			window.localStorage.setItem("resources", data.result);
		}
	});
	
	myApp.hideIndicator();
})

$(document).on('pageInit', '.page[data-page="blog"]', function (e) 
{
	myApp.showIndicator();

	var blog_items = window.localStorage.getItem("blog_items");
	$( "#blog_items" ).html( blog_items );
	
	service.get_blog_items().done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			$( "#blog_items" ).html( data.result );
			window.localStorage.setItem("blog_items", data.result);
		}
	});
	
	myApp.hideIndicator();
})

$(document).on('pageInit', '.page[data-page="blog-item"]', function (e) 
{
	$( "#blog_item" ).html( '' );
})

$(document).on('pageInit', '.page[data-page="invoice-page"]', function (e) 
{
	myApp.showIndicator();
	
	var event_id = window.localStorage.getItem("current_event_id");
	var booking_id = window.localStorage.getItem("booking_id"+event_id);
	var booking_cost = window.localStorage.getItem("booking_cost"+event_id);
	var booking_date = window.localStorage.getItem("booking_date"+event_id);
	var booking_event = window.localStorage.getItem("booking_event"+event_id);
	var booking_location = window.localStorage.getItem("booking_location"+event_id);
	var booking_number = window.localStorage.getItem("booking_number"+event_id);
	var booking_name = window.localStorage.getItem("booking_name"+event_id);
	
	$( "#user-name" ).html( booking_name );
	$( "#event-name" ).html( booking_event );
	$( "#event-date" ).html( booking_date );
	$( "#event-location" ).html( booking_location );
	$( "#event-cost" ).html( booking_cost );
	$( "#booking-number" ).html( booking_number );
	
	myApp.hideIndicator();
})

$(document).on('pageInit', '.page[data-page="pay-instructions-page"]', function (e) 
{
	myApp.showIndicator();
	
	var event_id = window.localStorage.getItem("current_event_id");
	var booking_id = window.localStorage.getItem("booking_id"+event_id);
	var booking_cost = window.localStorage.getItem("booking_cost"+event_id);
	var booking_date = window.localStorage.getItem("booking_date"+event_id);
	var booking_event = window.localStorage.getItem("booking_event"+event_id);
	var booking_location = window.localStorage.getItem("booking_location"+event_id);
	var booking_number = window.localStorage.getItem("booking_number"+event_id);
	var booking_name = window.localStorage.getItem("booking_name"+event_id);
	
	$( "#amount" ).html( '<strong>'+booking_cost+'</strong>' );
	$( "#account" ).html( '<strong>'+booking_number+'</strong>' );
	
	myApp.hideIndicator();
})

$(document).on('pageInit', '.page[data-page="services"]', function (e) 
{
	myApp.showIndicator();
	
	var resources = window.localStorage.getItem("services");
	$( "#services-list" ).html( resources );
	
	service.get_services().done(function (employees)
	{
		var data = jQuery.parseJSON(employees);
		
		if(data.message == "success")
		{
			$( "#services-list" ).html( data.result );
			window.localStorage.setItem("services", data.result);
		}
	});
	
	myApp.hideIndicator();
})
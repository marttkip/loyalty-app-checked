function onPhotoDataSuccess(imageURI) {
// Uncomment to view the base64-encoded image data
console.log(imageURI);
// Get image handle
//
var cameraImage = document.getElementById('image');
// Unhide image elements
//
cameraImage.style.display = 'block';
// Show the captured photo
// The inline CSS rules are used to resize the image
cameraImage.src = imageURI;
}
// Called when a photo is successfully retrieved
//
 
function onPhotoURISuccess(imageURI) {
// Uncomment to view the image file URI
console.log(imageURI);
// Get image handle
//
var galleryImage = document.getElementById('image');

// Unhide image elements
//
galleryImage.style.display = 'block';


var submit_order = document.getElementById('submit_order');
submit_order.style.display = 'block';

// Show the captured photo
// The inline CSS rules are used to resize the image
//
galleryImage.src = imageURI;

}


function onPhotoURISuccessAsset(imageURI) {
// Uncomment to view the image file URI
console.log(imageURI);
// Get image handle
//
var galleryImage = document.getElementById('image_asset');

// Unhide image elements
//
galleryImage.style.display = 'block';


var submit_order = document.getElementById('submit_asset');
submit_order.style.display = 'block';

// Show the captured photo
// The inline CSS rules are used to resize the image
//
galleryImage.src = imageURI;

}


function onPhotoURISuccessAddAsset(imageURI) {
// Uncomment to view the image file URI
console.log(imageURI);
// Get image handle
//
var galleryImage = document.getElementById('image_asset_new');

// Unhide image elements
//
galleryImage.style.display = 'block';


var submit_order = document.getElementById('submit_asset_new');
submit_order.style.display = 'block';

// Show the captured photo
// The inline CSS rules are used to resize the image
//
galleryImage.src = imageURI;

}
// A button will call this function
//
function captureAssetPhoto() {
// Take picture using device camera and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoURISuccessAsset, onFail, {
    quality: 30,
    targetWidth: 600,
    targetHeight: 600,
    destinationType: destinationType.FILE_URI,
    saveToPhotoAlbum: true
    });
}
function captureAddAssetPhoto() {
// Take picture using device camera and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoURISuccessAddAsset, onFail, {
    quality: 30,
    targetWidth: 600,
    targetHeight: 600,
    destinationType: destinationType.FILE_URI,
    saveToPhotoAlbum: true
    });
}



 
function capturePhoto() {
// Take picture using device camera and retrieve image as base64-encoded string
navigator.camera.getPicture(onPhotoDataSuccess, onFail, {
quality: 30,
targetWidth: 600,
targetHeight: 600,
destinationType: destinationType.FILE_URI,
saveToPhotoAlbum: true
});
}
// A button will call this function
//
 
function getPhoto(source) {
// Retrieve image file location from specified source
navigator.camera.getPicture(onPhotoURISuccess, onFail, {
quality: 30,
targetWidth: 600,
targetHeight: 600,
destinationType: destinationType.FILE_URI,
sourceType: source
});
}


function getAssetPhoto(source) {
// Retrieve image file location from specified source
navigator.camera.getPicture(onPhotoURISuccessAsset, onFail, {
quality: 30,
targetWidth: 600,
targetHeight: 600,
destinationType: destinationType.FILE_URI,
sourceType: source
});
}


function getAddAssetPhoto(source) {
// Retrieve image file location from specified source
navigator.camera.getPicture(onPhotoURISuccessAddAsset, onFail, {
quality: 30,
targetWidth: 600,
targetHeight: 600,
destinationType: destinationType.FILE_URI,
sourceType: source
});
}
// Called if something bad happens.
//
 
function onFail(message) {
//alert('Failed because: ' + message);
}
function upload() {
    var img = document.getElementById('image');

    var imageURI = img.src;
    var options = new FileUploadOptions();
    options.fileKey = "upload_image";
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";
    var params = new Object();
    options.params = params;
    options.chunkedMode = false;
    var ft = new FileTransfer();

    var company_id = window.localStorage.getItem('company_id');
    var personnel_id = window.localStorage.getItem('personnel_id');
    var feedback_category_id = document.getElementById('feedback_category_id');

    if(feedback_category_id == 0 || feedback_category_id == null)
    {
         myApp.alert("Please select a feedback category",'Salespro');
    }
    else
    {
        ft.upload(imageURI,encodeURI(base_url+"client/upload_file/"+personnel_id+"/"+company_id+"/"+feedback_category_id), win, fail,
        options);
    }

    
}

function competitor_upload(competitor_feedback_id) {
    var img = document.getElementById('image');

    var imageURI = img.src;
    var options = new FileUploadOptions();
    options.fileKey = "upload_image";
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";
    var params = new Object();
    options.params = params;
    options.chunkedMode = false;
    var ft = new FileTransfer();

    var personnel_id = window.localStorage.getItem('personnel_id');
    var feedback_category_id = document.getElementById('feedback_category_id');

    if(feedback_category_id == 0 || feedback_category_id == null)
    {
         myApp.alert("Please select a feedback category",'Salespro');
    }
    else
    {
        ft.upload(imageURI,encodeURI(base_url+"client/upload_competitor_file/"+personnel_id+"/"+feedback_category_id+"/"+competitor_feedback_id), win, fail,
        options);
    }

    
}


function submit_asset()
{

    var img = document.getElementById('image_asset');

    var imageURI = img.src;
    var options = new FileUploadOptions();
    options.fileKey = "upload_image";
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";
    var params = new Object();
    options.params = params;
    options.chunkedMode = false;
    var ft = new FileTransfer();

    var personnel_id = window.localStorage.getItem('personnel_id');
     var asset =  window.localStorage.getItem('asset_id');

     var app_latitude = window.localStorage.getItem('app_latitude');
    var app_longitude = window.localStorage.getItem('app_longitude');

    if(app_latitude == '' || app_longitude == '')
    {
         myApp.addNotification({
            title: 'Salespro App',
            message: 'Make sure your phone has internet connection and the gps is on'
        });

    }
    else
    {
        
        myApp.showPreloader('Submitting Asset Image ...');
        ft.upload(imageURI,encodeURI(base_url+"client/upload_asset_item/"+personnel_id+"/"+app_latitude+"/"+app_longitude+"/"+asset_id), Assetwin, fail,
        options);
        myApp.hidePreloader();
    }
   
    
}

function submit_order() {
    var img = document.getElementById('image');

    var imageURI = img.src;
    var options = new FileUploadOptions();
    options.fileKey = "upload_image";
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";
    var params = new Object();
    options.params = params;
    options.chunkedMode = false;
    var ft = new FileTransfer();

    var order_id = window.localStorage.getItem('order_id');
    var company_id = window.localStorage.getItem('company_id');
    var personnel_id = window.localStorage.getItem('personnel_id');
    myApp.showPreloader('Submitting Order ...');
    ft.upload(imageURI,encodeURI(base_url+"client/submit_client_order/"+company_id+"/"+personnel_id+"/"+order_id), order_win, fail,
        options);
    myApp.hidePreloader();
}



function order_win(r) {
    
    myApp.hidePreloader();
    myApp.closeModal();
    myApp.alert("Order successfully submited",'Salespro');

    var galleryImage = document.getElementById('image');
    galleryImage.style.display = 'none';
    var submit_order = document.getElementById('submit_order');
    submit_order.style.display = 'none';

    mainView.router.loadPage('orders.html');

    document.getElementById( "order_div" ).style.display = "none";
    document.getElementById( "create_order_button" ).style.display = "block";

    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}

function asset_add_win(r) {
    
    myApp.hidePreloader();
    myApp.closeModal();
    myApp.alert("Successfully added asset ",'Salespro');

    var galleryImage = document.getElementById('image_asset_new');
    galleryImage.style.display = 'none';
    var submit_order = document.getElementById('submit_asset_new');
    submit_order.style.display = 'none';

    mainView.router.loadPage('asset-tracker.html');

    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}

function Assetwin(r) {
    myApp.hidePreloader();
    myApp.closeModal();
    mainView.router.loadPage('asset-tracker.html');
    var galleryImage = document.getElementById('asset_image');
    galleryImage.style.display = 'none';
    var submit_order = document.getElementById('submit_asset');
    submit_order.style.display = 'none';


    myApp.alert("Asset Image uploaded successfully");
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}
function win(r) {
	myApp.alert("Image uploaded successfully");

    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}
 
function fail(error) {
    myApp.hidePreloader();
    myApp.alert("An error has occurred: Code = " + error.code);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
}
//login & registration functions
var Login_service = function () {

    var url;

    this.initialize = function (serviceURL) {
        url = serviceURL ? serviceURL : base_url;
        var deferred = $.Deferred();
        deferred.resolve();
        return deferred.promise();
    }

    this.findById = function (id) {
        return $.ajax({url: url + "/" + id});
    }

    this.register_member = function (form_data) {
        var request = url + "login/register_user";
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: "application/json; charset=utf-8", contentType: false});
    }
    this.login_member = function (login_member_no, login_member_password)
    {
        var request = url + "login/login_member/";
        //myApp.alert(request);
        return $.ajax({url: request, type: 'POST', data: {member_no: login_member_no, member_password: login_member_password}});
    }

    this.get_promotion_listings = function (personnel_id)
    {
        var request = url + "vehicle/get_all_vehicles/" + personnel_id;
        return $.ajax({url: request});
    }
    this.get_all_product_purchases = function (personnel_id)
    {
        var request = url + "vehicle/get_all_transactions/" + personnel_id;
        return $.ajax({url: request});
    }
    this.query_vehicle_parked = function (form_data) {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "vehicle/query_vehicle_parked/" + personnel_id;
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: "application/json; charset=utf-8", contentType: false});
    }
    this.get_all_parking_zones = function ()
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "vehicle/get_all_parking_zones/" + personnel_id;
        return $.ajax({url: request});
    }

    this.get_zone_parking_lot = function (zone_id)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "vehicle/get_all_parking_lots/" + personnel_id + "/" + zone_id;
        return $.ajax({url: request});
    }
    this.get_sub_zone = function (parking_lot_id)
    {
        var request = url + "vehicle/get_all_parking_sub_zones/" + parking_lot_id;
        return $.ajax({url: request});
    }

    this.get_customer_details = function (customer_id)
    {
        var request = url + "vehicle/get_customer_details/" + customer_id;
        return $.ajax({url: request});
    }
    this.clamp_vehicle = function (personnel_id, plate_check_id, clamp_number, clamping_id)
    {
        var request = url + "vehicle/clamp_vehicle/" + personnel_id + "/" + plate_check_id + "/" + clamp_number + "/" + clamping_id;
        return $.ajax({url: request});
    }
    this.declamp_vehicle = function (personnel_id, plate_check_id, clamp_number, clamping_id)
    {
        var request = url + "vehicle/declamp_vehicle/" + personnel_id + "/" + plate_check_id + "/" + clamp_number + "/" + clamping_id;
        return $.ajax({url: request});
    }
    this.tow_vehicle = function (personnel_id, plate_check_id, clamping_id)
    {
        var request = url + "vehicle/tow_vehicle/" + personnel_id + "/" + plate_check_id + "/" + clamping_id;
        return $.ajax({url: request});
    }
    this.get_all_vehicle_types = function ()
    {
        var request = url + "vehicle/get_all_vehicle_types";
        return $.ajax({url: request});
    }



    this.add_reminder = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "reminders/add_reminder/" + personnel_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    this.change_password = function (form_data) {
        var request = url + "login/change_password";
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    this.book_non_member = function (form_data)
    {
        var event_id = window.localStorage.getItem("current_event_id");
        var registrationId = window.localStorage.getItem("iod_registrationId");

        var request = url + "booking/book_non_member/" + event_id + '/' + registrationId;
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    this.book_member = function (member_id)
    {
        var event_id = window.localStorage.getItem("current_event_id");
        var registrationId = window.localStorage.getItem("iod_registrationId");
        var request = url + "booking/book_member/" + event_id + '/' + member_id + '/' + registrationId;
        return $.ajax({url: request});
    }

    this.getProfileDetails = function () {
        var request = url + "login/get_client_profile";
        return $.ajax({url: request});
    }

    this.get_forum_items = function ()
    {
        var request = url + "forum/get_blog_items";
        return $.ajax({url: request});
    }

    this.get_member_invoices = function (member_id)
    {
        var request = url + "forum/get_member_invoices/" + member_id;
        return $.ajax({url: request});
    }

    this.get_reminders = function (personnel_id)
    {
        var request = url + "reminders/get_reminders/" + personnel_id;
        return $.ajax({url: request});
    }

    this.get_calendar_items = function ()
    {
        var request = url + "events/calendar";
        return $.ajax({url: request});
    }

    this.get_event_details = function (id)
    {
        var request = url + "events/get_calendar_details/" + id;
        return $.ajax({url: request});
    }

    this.get_blog_items = function ()
    {
        var request = url + "news/get_blog_items";
        return $.ajax({url: request});
    }

    this.get_blog_item = function (id)
    {
        var request = url + "news/get_news_detail/" + id;
        return $.ajax({url: request});
    }

    this.get_resources = function ()
    {
        var request = url + "events/get_resources";
        return $.ajax({url: request});
    }

    this.getBlogDetail = function (id, member_id) {
        var request = url + "forum/get_forum_comments";
        return $.ajax({url: url + "forum/get_forum_comments/" + id + "/" + member_id});
    }

    this.add_discussion = function (form_data) {
        var request = url + "forum/add_discussion";
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    this.add_comment = function (comment, post_id, member_id) {
        var request = url + "forum/add_comment";
        return $.ajax({url: request, data: {comment: comment, post_id: post_id, member_id: member_id}, type: 'POST'});
    }

    this.add_registration_id = function (to) {
        var request = url + "login/add_registration_id/" + to;
        return $.ajax({url: request});
    }

    this.get_services = function ()
    {
        var request = url + "events/get_services";
        return $.ajax({url: request});
    }
    this.get_profile = function (personnel_id)
    {
        var request = url + "login/get_profile/" + personnel_id;
        //myApp.alert(request);
        return $.ajax({url: request});
    }
    this.get_contributions = function ()
    {
        var request = url + "profile/get_contributions/2";
        //myApp.alert(request);
        return $.ajax({url: request});
    }




    // leads

    this.get_leads = function (personnel_id)
    {
        var request = url + "lead/get_leads/" + personnel_id;
        return $.ajax({url: request});
    }

    this.add_lead = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var app_latitude = window.localStorage.getItem('app_latitude');
        var app_longitude = window.localStorage.getItem('app_longitude');
        var request = url + "client/add_client/" + personnel_id + "/" + app_latitude + "/" + app_longitude;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    // clients
    this.get_clients = function (personnel_id)
    {
        var request = url + "client/get_clients/" + personnel_id;
        return $.ajax({url: request});
    }
    this.get_every_clients = function (personnel_id)
    {
        var request = url + "client/get_every_clients/" + personnel_id;
        return $.ajax({url: request});
    }
    this.get_every_competitor = function (personnel_id)
    {
        var request = url + "client/get_every_competitor/" + personnel_id;
        return $.ajax({url: request});
    }
    this.get_client_profile = function (personnel_id, company_id)
    {
        var request = url + "client/get_client_profile/" + personnel_id + "/" + company_id;
        return $.ajax({url: request});
    }
    this.get_client_info = function (personnel_id, company_id)
    {
        var request = url + "client/get_client_info/" + personnel_id + "/" + company_id;
        return $.ajax({url: request});
    }

    this.get_company_stocks = function (personnel_id, company_id)
    {
        var request = url + "stock/get_all_products/" + personnel_id + "/" + company_id;
        return $.ajax({url: request});
    }
    this.get_product_detail = function (product_id)
    {
        var request = url + "stock/get_product_detail/" + product_id;
        return $.ajax({url: request});
    }
    this.get_competition_product_detail = function (product_id)
    {
        var request = url + "competition/get_competition_product_detail/" + product_id;
        return $.ajax({url: request});
    }

    this.add_stock_record = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var company_id = window.localStorage.getItem("company_id");

        var request = url + "stock/add_stock/" + personnel_id + "/" + company_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    this.get_company_orders = function (personnel_id, company_id, order_id)
    {
        var request = url + "order/get_all_products/" + personnel_id + "/" + company_id + "/" + order_id;
        return $.ajax({url: request});
    }
    this.check_active_order = function (personnel_id, company_id)
    {
        var request = url + "order/check_active_order/" + personnel_id + "/" + company_id;
        return $.ajax({url: request});
    }
    this.create_new_order = function (personnel_id, company_id)
    {
        var request = url + "order/create_new_order/" + personnel_id + "/" + company_id;
        return $.ajax({url: request});
    }

    this.add_order_record = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var company_id = window.localStorage.getItem("company_id");
        var order_id = window.localStorage.getItem("order_id");

        var request = url + "order/add_order/" + personnel_id + "/" + company_id + "/" + order_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    this.add_competitor_details = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");

        var request = url + "client/add_competitor/" + personnel_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }
    this.adding_competition = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");

        var request = url + "client/add_competition/" + personnel_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }
    this.add_activity = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");

        var request = url + "client/add_activity_details/" + personnel_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }
    this.get_company_sales = function (personnel_id, company_id)
    {
        var request = url + "sale/get_all_products/" + personnel_id + "/" + company_id;
        return $.ajax({url: request});
    }
    this.get_feedback_details = function (personnel_id, company_id)
    {
        var request = url + "client/get_feedback_details/" + personnel_id + "/" + company_id;
        return $.ajax({url: request});
    }

    this.get_feedback_items = function (personnel_id)
    {
        var request = url + "client/get_feedback_items/" + personnel_id;
        return $.ajax({url: request});
    }
    this.add_sale_record = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var company_id = window.localStorage.getItem("company_id");

        var request = url + "sale/add_sale/" + personnel_id + "/" + company_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }
    this.get_competiting_products = function ()
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "competition/get_all_competing_products/" + personnel_id;
        return $.ajax({url: request});
    }

    this.get_competiting_stock_products = function ()
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "competition/get_all_competing_stock_products/" + personnel_id;
        return $.ajax({url: request});
    }

    this.add_competition_record = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");

        var request = url + "competition/add_competition_record/" + personnel_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    this.add_competition_stock_record = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");

        var request = url + "competition/add_competition_stock_record/" + personnel_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }


    // leads

    this.get_expenses = function (personnel_id)
    {
        var request = url + "expense/get_expenses/" + personnel_id;
        return $.ajax({url: request});
    }

    this.add_expense = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "expense/add_expense/" + personnel_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }


    this.schedule_visit_today = function (personnel_id, client_id)
    {
        var request = url + "client/schedule_visit/" + personnel_id + "/" + client_id;
        return $.ajax({url: request});
    }

    // appointments
    this.add_schedule = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var company_id = window.localStorage.getItem("company_id");

        var request = url + "appointment/add_appointment/" + personnel_id + "/" + company_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }
    this.add_client_schedule = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var company_id = window.localStorage.getItem("client_id_detail");

        var request = url + "appointment/add_appointment/" + personnel_id + "/" + company_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }
    this.get_all_schedules = function (personnel_id, company_id)
    {
        var request = url + "appointment/get_appointments/" + personnel_id + "/" + company_id;
        return $.ajax({url: request});
    }


    // location tracker
    this.reverse_geocode = function (latitude, longitude)
    {
        var company_id = window.localStorage.getItem("company_id");
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "client/reverse_geocode/" + latitude + "/" + longitude + "/" + company_id + "/" + personnel_id;
        return $.ajax({url: request});
    }
    this.start_visit_journey = function (latitude, longitude)
    {
        var company_id = window.localStorage.getItem("company_id");
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "client/start_client_visit/" + latitude + "/" + longitude + "/" + company_id + "/" + personnel_id;
        return $.ajax({url: request});
    }



    this.get_all_assets = function (personnel_id)
    {
        var request = url + "asset/get_all_assets/" + personnel_id;
        return $.ajax({url: request});
    }
    this.get_asset_detail = function (asset_id, client_id, app_latitude, app_longitude)
    {
        var request = url + "asset/get_asset_detail/" + asset_id + "/" + client_id + "/" + app_latitude + "/" + app_longitude;
        return $.ajax({url: request});
    }

    this.submit_current_location = function (personnel_id, app_latitude, app_longitude)
    {
        var request = url + "client/submit_current_location/" + personnel_id + "/" + app_latitude + "/" + app_longitude;
        return $.ajax({url: request});
    }


    this.get_all_promotions = function (personnel_id)
    {
        var request = url + "promotion/get_all_promotions/" + personnel_id;
        return $.ajax({url: request});
    }
    this.get_promotion_detail = function (promotion_id)
    {
        var request = url + "promotion/get_promotion_detail/" + promotion_id;
        return $.ajax({url: request});
    }

    this.get_all_persons = function (personnel_id, promotion_id)
    {
        var request = url + "promotion/get_all_persons/" + personnel_id + "/" + promotion_id;
        return $.ajax({url: request});
    }
    this.get_person_promotion_item = function (person_id, promotion_id)
    {
        var request = url + "promotion/get_person_promotion_item/" + person_id + "/" + promotion_id;
        return $.ajax({url: request});
    }

    this.award_promotion_item = function (person_id, promotion_id, promotion_item_id)
    {

        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "promotion/award_promotion_item/" + person_id + "/" + promotion_id + "/" + personnel_id + "/" + promotion_item_id;
        return $.ajax({url: request});

    }
    this.delete_promotion_item = function (person_promotion_item_id)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "promotion/delete_promotion_item/" + person_promotion_item_id;
        return $.ajax({url: request});

    }

    this.add_promotion_person = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var app_latitude = window.localStorage.getItem('app_latitude');
        var app_longitude = window.localStorage.getItem('app_longitude');
        var promotion_id = window.localStorage.getItem('promotion_id');
        var request = url + "promotion/add_promotion_person/" + personnel_id + "/" + app_latitude + "/" + app_longitude + "/" + promotion_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }


    this.get_posm_items = function (personnel_id, company_id)
    {
        var request = url + "posm/get_all_posm_items/" + personnel_id + "/" + company_id;
        return $.ajax({url: request});
    }
    this.get_posm_detail = function (posm_id)
    {
        var request = url + "posm/get_posm_detail/" + posm_id;
        return $.ajax({url: request});
    }


    this.add_posm_record = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var company_id = window.localStorage.getItem("company_id");

        var request = url + "posm/add_posm/" + personnel_id + "/" + company_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    this.get_survey = function (personnel_id)
    {
        var request = url + "survey/get_all_survey/" + personnel_id;
        return $.ajax({url: request});
    }
    this.get_survey_questions = function (survey_id)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var request = url + "survey/get_all_survey_questions/" + survey_id + "/" + personnel_id;
        return $.ajax({url: request});
    }

    this.add_survey_answers = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");

        var request = url + "survey/add_survey_answers/" + personnel_id;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }

    this.add_asset_details = function (form_data)
    {
        var personnel_id = window.localStorage.getItem("personnel_id");
        var app_latitude = window.localStorage.getItem('app_latitude');
        var app_longitude = window.localStorage.getItem('app_longitude');

        var request = url + "asset/add_asset_details/" + personnel_id + "/" + app_latitude + "/" + app_longitude;

        return $.ajax({url: request, data: form_data, type: 'POST', processData: false, contentType: false});
    }
}
var service = new Login_service();
service.initialize().done(function () {
    console.log("Service initialized");
});
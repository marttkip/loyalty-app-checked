//login & registration functions
var Login_service = function() {

    var url;

    this.initialize = function(serviceURL) {
        url = serviceURL ? serviceURL : base_url;
        var deferred = $.Deferred();
        deferred.resolve();
        return deferred.promise();
    }

    this.findById = function(id) {
        return $.ajax({url: url + "/" + id});
    }

    this.register_member = function(form_data) {
		var request = url + "login/register_user";
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false,contentType: false});
    }
    this.login_member = function(login_member_no, login_member_password) {
		/*var request = url + "forum/login_member_new";
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false,contentType: false});*/
		
		var request = url + "login/login_member/";
		return $.ajax({url: request, type: 'POST', data: {member_no: login_member_no, member_password: login_member_password }});
		
		return $.post( request, {member_no: login_member_no, member_password: login_member_password });
	
		/*var request = url + "login/login_member/"+login_member_no+'/'+login_member_password ;
		alert(request);
		return $.ajax({url: request});*/
    }
	
    this.change_password = function(form_data) {
		var request = url + "login/change_password";
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false,contentType: false});
    }
	
    this.book_non_member = function(form_data) 
	{
		var event_id = window.localStorage.getItem("current_event_id");
		var registrationId = window.localStorage.getItem("iod_registrationId");
		
		var request = url + "booking/book_non_member/"+event_id+'/'+registrationId;
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false,contentType: false});
    }
	
    this.book_member = function(member_id)
	{
		var event_id = window.localStorage.getItem("current_event_id");
		var registrationId = window.localStorage.getItem("iod_registrationId");
		var request = url + "booking/book_member/"+event_id+'/'+member_id+'/'+registrationId;
		return $.ajax({url: request});
    }
	
    this.getProfileDetails = function() {
		var request = url + "login/get_client_profile";
        return $.ajax({url: request});
    }
	
	this.get_forum_items = function()
	{
		var request = url + "forum/get_blog_items" ;
		return $.ajax({url: request});
	}
	
	this.get_member_invoices = function(member_id)
	{
		var request = url + "forum/get_member_invoices/"+member_id;
		return $.ajax({url: request});
	}
	
	this.get_calendar_items = function()
	{
		var request = url + "events/calendar";
		return $.ajax({url: request});
	}
	
	this.get_event_details = function(id)
	{
		var request = url + "events/get_calendar_details/"+id;
		return $.ajax({url: request});
	}
	
	this.get_blog_items = function()
	{
		var request = url + "news/get_blog_items";
		return $.ajax({url: request});
	}
	
	this.get_blog_item = function(id)
	{
		var request = url + "news/get_news_detail/"+id;
		return $.ajax({url: request});
	}
	
	this.get_resources = function()
	{
		var request = url + "events/get_resources";
		return $.ajax({url: request});
	}

    this.getBlogDetail = function(id, member_id) {
		var request = url + "forum/get_forum_comments" ;
        return $.ajax({url: url + "forum/get_forum_comments/" + id + "/" + member_id});
    }
	
    this.add_discussion = function(form_data) {
		var request = url + "forum/add_discussion";
        return $.ajax({url: request, data: form_data, type: 'POST', processData: false,contentType: false});
    }
	
    this.add_comment = function(comment, post_id, member_id) {
		var request = url + "forum/add_comment" ;
        return $.ajax({url: request, data: {comment :comment, post_id: post_id, member_id: member_id}, type: 'POST'});
    }
	
    this.add_registration_id = function(to) {
		MSApp.execUnsafeLocalFunction(function () {
			var request = url + "login/add_registration_id/"+to;
			return $.ajax({url: request});
		});
    }
	
	this.get_services = function()
	{
		var request = url + "events/get_services";
		return $.ajax({url: request});
	}

}
var service = new Login_service();
service.initialize().done(function () {
	console.log("Service initialized");
});